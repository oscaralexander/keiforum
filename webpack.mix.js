const mix = require('laravel-mix');

mix.js('src/js/app.js', 'web/assets/js')
    .sass('src/css/app.scss', 'web/assets/css')
    .sourceMaps()
    .browserSync({
        proxy: 'keiforum.test:7888',
        files: [
            'app/views/*',
            'web/assets/css/*.css',
            'web/assets/js/*.js',
        ],
    });
