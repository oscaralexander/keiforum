<?php

namespace App\Actions\Auth;

use App\Actions\Action;
use App\Models\User;
use Core\Auth;
use Core\Response;
use Core\View;

class ResetPasswordAction extends Action
{
    public function run(): string
    {
        $token = $this->router->getParam('token');
        $user = User::getFirstWhere(['password_reset_token' => $token]);

        if ($user) {
            return View::render('auth/reset_password', [
                'token' => $token,
            ]);
        }

        return Response::error(404);
    }
}
