<?php

namespace App\Actions\Auth;

use App\Actions\Action;
use App\Models\User;
use Core\Auth;
use Core\Response;
use Core\Validator;

class LoginPostAction extends Action
{
    public function run(): string
    {
        $username_or_email = u_get('username_or_email', $_POST);
        $password = u_get('password', $_POST);

        $post_data = [
            'username_or_email' => $username_or_email,
            'password' => $password,
        ];

        $validator = new Validator([
            'username_or_email' => [
                'required' => [
                    'error' => 'Gebruikersnaam of e-mailadres is verplicht.',
                ],
            ],
            'password' => [
                'required' => [
                    'error' => 'Wachtwoord is verplicht.',
                ],
            ],
        ]);

        if ($validator->validate($post_data)) {
            $col = filter_var($username_or_email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

            if ($user = User::getFirstWhere([$col => $username_or_email])) {
                if (password_verify($password, $user->password)) {
                    if ($user->is_active) {
                        $referrer = u_get('referrer', $_SESSION, '/');
                        $remember = u_get('remember', $_POST);
                        $remember_token = null;
                        unset($_SESSION['referrer']);

                        if ($remember == '1') {
                            User::update([
                                'remember_token' => User::getUniqueRememberToken(),
                            ], $user->id);
                        }

                        Auth::login($user->id, $remember_token);
                        // return Response::redirect($referrer);
                        return Response::json($_POST);
                    } else {
                        $validator->addError('username_or_email', 'Dit account is nog niet geactiveerd.');
                    }
                }
            }

            $validator->addError('password', 'Inlogpoging mislukt.');
        }

        return Response::redirect('/inloggen', [
            'errors' => $validator->getErrors(),
        ], 422);
    }
}
