<?php

namespace App\Actions\Auth;

use App\Actions\Action;
use App\Models\User;
use Core\Mail;
use Core\Response;
use Core\Validator;

class ForgotPasswordPostAction extends Action
{
    public function run(): string
    {
        $username_or_email = u_get('username_or_email', $_POST);

        $post_data = [
            'username_or_email' => $username_or_email,
        ];

        $validator = new Validator([
            'username_or_email' => [
                'required' => [
                    'error' => 'Gebruikersnaam of e-mailadres is verplicht.',
                ],
            ],
        ]);

        if ($validator->validate($post_data)) {
            $col = filter_var($username_or_email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

            if ($user = User::getFirstWhere([$col => $username_or_email])) {
                $password_reset_token = User::getUniquePasswordResetToken();
                User::update(['password_reset_token' => $password_reset_token], $user->id);
                $user->password_reset_token = $password_reset_token;

                try {
                    $mail = new Mail();
                    $mail->addRecipient($user->email);
                    $mail->setSubject('Wijzig je wachtwoord');
                    $mail->setBody(u_view('_mail/auth/reset_password', ['user' => $user], null));
                    $mail->send();
                } catch (\Exception $e) {
                    return Response::error(500, ['exception' => $e]);
                }
            }

            return Response::jsonOrRedirect('/wachtwoord-vergeten', ['success' => true]);
        }

        return Response::redirect('/wachtwoord-vergeten', [
            'errors' => $validator->getErrors(),
        ], 422);
    }
}
