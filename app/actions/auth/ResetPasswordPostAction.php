<?php

namespace App\Actions\Auth;

use App\Actions\Action;
use App\Models\User;
use Core\Auth;
use Core\Response;
use Core\Validator;
use Core\View;

class ResetPasswordPostAction extends Action
{
    public function run(): string
    {
        $token = u_get('token', $_POST);
        $password = u_get('password', $_POST);
        $password_confirm = u_get('password_confirm', $_POST);

        $data = [
            'password' => $password,
            'password_confirm' => $password_confirm,
        ];

        $validator = new Validator([
            'password' => [
                'required' => 'Wachtwoord is verplicht.',
                'password' => 'Wachtwoord is niet veilig genoeg.',
                'confirmed' => 'Wachtwoord is onjuist herhaald.',
            ],
        ]);

        if ($user = User::getFirstWhere(['password_reset_token' => $token])) {
            if ($validator->validate($data)) {
                User::update([
                    'password' => password_hash($data['password'], PASSWORD_DEFAULT),
                    'password_reset_token' => null,
                ], $user->id);

                Auth::login($user->id);
                return Response::redirect('/wachtwoord-gewijzigd');
            }
        }

        return Response::error(404);
    }
}
