<?php

namespace App\Actions\Auth;

use App\Actions\Action;
use App\Models\User;
use Core\Auth;
use Core\Response;

class LogoutPostAction extends Action
{
    public function run(): string
    {
        if (Auth::check()) {
            $user = Auth::getUser();
            User::update([
                'remember_token' => User::getUniqueRememberToken(),
            ], $user->id);
            Auth::logout();
        }

        Response::redirect('/inloggen');
    }
}
