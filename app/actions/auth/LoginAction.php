<?php

namespace App\Actions\Auth;

use App\Actions\Action;
use App\Models\User;
use Core\Auth;
use Core\Response;
use Core\View;

class LoginAction extends Action
{
    public function run(): string
    {
        if (empty($_SESSION['referrer'])) {
            if ($referrer = u_get('HTTP_REFERER', $_SERVER)) {
                $_SESSION['referrer'] = $referrer;
            }
        }

        if (Auth::check()) {
            return Response::redirect(u_get('referrer', $_SESSION, '/'));
        }

        return View::render('auth/login');
    }
}
