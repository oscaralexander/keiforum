<?php

namespace App\Actions\Auth;

use App\Actions\Action;
use Core\View;

class ForgotPasswordAction extends Action
{
    public function run(): string
    {
        return View::render('auth/forgot_password');
    }
}
