<?php

namespace App\Actions\Auth;

use App\Actions\Action;
use Core\View;

class ResetPasswordSuccessAction extends Action
{
    public function run(): string
    {
        return View::render('auth/reset_password_success');
    }
}
