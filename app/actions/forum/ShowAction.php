<?php

namespace App\Actions\Forum;

use App\Actions\Action;
use App\Models\Forum;
use App\Models\Post;
use App\Models\Thread;
use App\Models\User;
use Core\Response;
use Core\View;

class ShowAction extends Action
{
    public function run(): string
    {
        $page = intval($this->router->getParam('page', 1));
        $slug = $this->router->getParam('slug');

        // Get forum
        $forum = Forum::findBySlug($slug);

        if (!$forum) {
            return Response::error(404);
        }

        // Get thread count
        $threads_total = Thread::count([
            'forum_id' => $forum->id,
            'is_deleted' => 0,
        ]);

        $num_pages = intval(ceil($threads_total / Thread::THREADS_PER_PAGE));

        if ($page > $num_pages) {
            return Response::error(404);
        }

        // Get forum threads
        $threads = Thread::getWhere([
            'forum_id' => $forum->id,
            'is_deleted' => 0,
        ], null, Thread::THREADS_PER_PAGE, $page - 1);

        // Get last posts
        $last_post_ids = array_column($threads, 'last_post_id');
        $last_posts = Post::findAll($last_post_ids);
        $last_posts = u_array_indexed($last_posts);

        // Get users
        $last_post_user_ids = array_column($last_posts, 'user_id');
        $user_ids = array_column($threads, 'user_id');
        $user_ids = array_merge($user_ids, $last_post_user_ids);
        $users = User::findAll($user_ids);
        $users = u_array_indexed($users);

        // Set relations
        foreach ($threads as &$thread) {
            $thread->user = $users['id_' . $thread->user_id] ?? null;
            $thread->last_post = $last_posts['id_' . $thread->last_post_id] ?? null;

            if ($thread->last_post) {
                $thread->last_post->user = $users['id_' . $thread->last_post->user_id] ?? null;
            }
        }

        return View::render('forum/show', [
            'forum' => $forum,
            'num_pages' => $num_pages,
            'page' => $page,
            'threads' => $threads,
        ]);
    }
}
