<?php

namespace App\Actions\Thread;

use App\Actions\Action;
use App\Models\Post;
use App\Models\Thread;
use Core\Auth;
use Core\Response;
use Core\Validator;
use Core\View;

class StoreAction extends Action
{
    protected $protected = true;

    public function run(): string
    {
        $current_user = Auth::getUser();
        $data = [
            'area_id' => u_get('area_id', $_POST),
            'forum_id' => u_get('forum_id', $_POST),
            'title' => u_get('title', $_POST, ''),
            'text' => u_get('text', $_POST, ''),
        ];

        if ($data['area_id'] == 0) {
            $data['area_id'] = null;
        }

        $validator = new Validator([
            'forum_id' => [
                'required' => 'Forum is verplicht.',
                'exists' => [
                    'arg' => 'forums.id',
                    'error' => 'Onbekend forum geselecteerd.',
                ],
            ],
            'area_id' => [
                '?exists' => [
                    'arg' => 'areas.id',
                    'error' => 'Onbekende wijk geselecteerd.',
                ],
            ],
            'title' => [
                'required' => 'Titel is verplicht.',
            ],
            'text' => [
                'required' => 'Bericht is verplicht.',
            ],
        ]);

        if ($validator->validate($data)) {
            $thread_data = [
                'area_id' => $data['area_id'],
                'forum_id' => $data['forum_id'],
                'user_id' => $current_user->id,
                'title' => $data['title'],
            ];

            if ($thread = Thread::create($thread_data)) {
                $post_data = [
                    'thread_id' => $thread->id,
                    'user_id' => $current_user->id,
                    'text' => $data['text'],
                ];

                if ($post = Post::create($post_data)) {
                    return Response::redirect($thread->url);
                }
            }
        } else {
            return Response::jsonOrRedirect('/nieuw-onderwerp', [
                'errors' => $validator->getErrors(),
            ], 422);
        }
    }
}
