<?php

namespace App\Actions\Thread;

use App\Actions\Action;
use App\Models\Area;
use App\Models\Forum;
use App\Models\Like;
use App\Models\Post;
use App\Models\Thread;
use App\Models\User;
use Core\Auth;
use Core\Data;
use Core\Response;
use Core\Router;
use Core\View;

class ShowAction extends Action
{
    public function run(): string
    {
        $id = intval($this->router->getParam('id'));
        $page = intval($this->router->getParam('page', 1));

        // Get thread
        $thread = Thread::find($id);

        if (!$thread) {
            return Response::error(404);
        }

        // Get forum
        $forum = Forum::find($thread->forum_id);

        // Get posts count
        $posts_total = Post::count(['thread_id' => $id]);
        $num_pages = intval(ceil($posts_total / Post::POSTS_PER_PAGE));

        if ($page > $num_pages) {
            return Response::error(404);
        }

        // Get posts
        $posts = Post::getWhere([
            'thread_id' => $id
        ], null, Post::POSTS_PER_PAGE, $page - 1);

        // Get users
        $users = $thread->getUsers();
        $users = u_array_indexed($users);

        // Set relations
        foreach ($posts as &$post) {
            $post->user = $users['id_' . $post->user_id] ?? null;
        }

        // Get user likes
        if ($current_user = Auth::getUser()) {
            $post_ids = array_column($posts, 'id');
            $user_likes = array_column(Like::getWhere([
                'post_id' => $post_ids,
                'user_id' => $current_user->id,
            ]), 'post_id');
        } else {
            $user_likes = [];
        }

        return View::render('thread/show', [
            'num_pages' => $num_pages,
            'page' => $page,
            'path' => [
                ['href' => $forum->url, 'label' => $forum->name],
            ],
            'posts' => $posts,
            'posts_per_page' => Post::POSTS_PER_PAGE,
            'thread' => $thread,
            'user_likes' => $user_likes,
            'users' => $users,
        ]);
    }
}
