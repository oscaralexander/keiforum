<?php

namespace App\Actions\Thread;

use App\Actions\Action;
use App\Models\Forum;
use Core\View;

class CreateAction extends Action
{
    public function run(): string
    {
        $area_id = u_old('area_id', u_get('area_id', $_GET, null));
        $forum = null;
        $forum_id = u_old('forum_id', u_get('forum_id', $_GET, null));

        if ($forum_id) {
            $forum = Forum::find($forum_id);
        }

        return View::render('thread/create', [
            'area_id' => $area_id,
            'forum' => $forum,
            'forum_id' => $forum_id,
        ]);
    }
}
