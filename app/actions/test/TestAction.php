<?php

namespace App\Actions\Test;

use App\Actions\Action;
use App\Models\User;
use Core\Mail;
use Core\Router;
use Core\View;

class TestAction extends Action
{
    public function run(Router $router): string
    {
        $bindings = [
            'alexander',
            'griffioen',
        ];

        $message = $this->mod($bindings);

        print_r($bindings);
        die($message);

        return View::render('test/test');
    }

    protected static function mod(array &$bindings): string
    {
        $bindings = array_merge($bindings, ['oscar']);
        return 'OK';
    }
}
