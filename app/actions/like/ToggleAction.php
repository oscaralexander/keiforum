<?php

namespace App\Actions\Like;

use App\Actions\Action;
use App\Models\Like;
use App\Models\Post;
use App\Models\User;
use Core\Auth;
use Core\Request;
use Core\Response;

class ToggleAction extends Action
{
    protected $protected = true;

    public function run(): string
    {
        if (Request::isAjax()) {
            $current_user = Auth::getUser();
            $post_id = intval(u_get('post_id', $_POST));

            if ($current_user) {
                if ($post = Post::find($post_id)) {
                    $increment = 0;
                    $likes = $post->getLikes();
                    $likes_count = count($likes);
                    $user_ids = array_column($likes, 'user_id');

                    if (in_array($current_user->id, $user_ids)) {
                        Like::delete([
                            'post_id' => $post->id,
                            'user_id' => $current_user->id,
                        ]);
                        $increment = -1;
                    } else {
                        Like::create([
                            'post_id' => $post->id,
                            'user_id' => $current_user->id,
                        ]);
                        $increment = 1;
                    }

                    Post::update(['likes_count' => $likes_count + $increment], $post->id);

                    return Response::json([
                        'increment' => $increment,
                        'likes_count' => $likes_count + $increment,
                    ]);
                }
            }
        }

        return Response::error(403);
    }
}
