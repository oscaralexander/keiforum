<?php

namespace App\Actions;

use Core\Auth;
use Core\Cache;
use Core\Data;
use Core\Interfaces\ActionInterface;
use Core\Router;
use App\Models\Admin;
use App\Models\Area;
use App\Models\Forum;
use App\Models\Moderator;

abstract class Action implements ActionInterface
{
    protected $protected = false;
    protected $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
        
        // Get logged in user
        $current_user = Auth::getUser();

        if ($current_user) {
            $current_user->updateLastActiveAt();
        }

        // Get admins
        if (!$admins = Cache::get('models/admins')) {
            $admins = Admin::all();
            Cache::set('models/admins', $admins, 60 * 60 * 24);
        }
        
        // Get areas
        if (!$areas = Cache::get('models/areas')) {
            $areas = Area::all();
            $areas = u_array_indexed($areas);
            Cache::set('models/areas', $areas, 60 * 60 * 24);
        }
        
        // Get forums
        if (!$forums = Cache::get('models/forums')) {
            $forums = Forum::all();
            $forums = u_array_indexed($forums);
            Cache::set('models/forums', $forums, 60 * 60 * 24);
        }

        // Get moderators
        if (!$moderators = Cache::get('models/moderators')) {
            $moderators = Moderator::all();
            Cache::set('models/moderators', $moderators, 60 * 60 * 24);
        }

        Data::set([
            'admins' => $admins,
            'areas' => $areas,
            'current_user' => $current_user,
            'forums' => $forums,
            'moderators' => $moderators,
        ]);
    }

    public function isProtected(): bool
    {
        return $this->protected;
    }
}
