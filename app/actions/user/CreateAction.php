<?php

namespace App\Actions\User;

use App\Actions\Action;
use Core\View;

class CreateAction extends Action
{
    public function run(): string
    {
        return View::render('user/create');
    }
}
