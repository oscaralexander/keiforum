<?php

namespace App\Actions\User;

use App\Actions\Action;
use App\Models\Area;
use App\Models\User;
use Core\Auth;
use Core\Request;
use Core\Response;
use Core\View;

class ActivateAction extends Action
{
    public function run(): string
    {
        $id = $this->router->getParam('id');
        $token = $this->router->getParam('token');
        
        // Get user
        $user = User::find($id);

        if (!$user) {
            return Response::error(404);
        }

        // http://localhost:3000/gebruiker/activeren/20/w7yjs1wx7J6LJnB8XvuUwTts
        
        if ($user->token === $token) {
            $token = User::getUniqueToken();
            $remember_token = User::getUniqueRememberToken();

            User::update([
                'is_active' => 1,
                'remember_token' => $remember_token,
                'token' => $token,
            ], $user->id);

            // Log in
            Auth::login($user->id, $remember_token);

            return View::render('user/activate_success', [
                'user' => $user,
            ]);
        }
    }
}
