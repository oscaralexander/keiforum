<?php

namespace App\Actions\User;

use App\Actions\Action;
use App\Models\User;
use Core\Mail;
use Core\Response;
use Core\Validator;

class StoreAction extends Action
{
    public function run(): string
    {
        $post_data = [
            'username' => u_get('username', $_POST),
            'email' => u_get('email', $_POST),
            'password' => u_get('password', $_POST),
            'terms' => u_get('terms', $_POST),
        ];

        $validator = new Validator([
            'username' => [
                'required' => 'Gebruikersnaam is verplicht.',
                'unique' => [
                    'arg' => 'users',
                    'error' => 'Deze gebruikersnaam is helaas al bezet.',
                ],
            ],
            'email' => [
                'required' => 'E-mailadres is verplicht.',
                'email' => 'Ongeldig e-mailadres.',
                'unique' => [
                    'arg' => 'users',
                    'error' => 'Dit e-mailadres heeft al een account.',
                ],
            ],
            'password' => [
                'required' => 'Wachtwoord is verplicht.',
                'password' => 'Wachtwoord is niet veilig genoeg.',
            ],
            'terms' => [
                'accepted' => 'De algemene voorwaarden moeten geaccepteerd worden.',
            ],
        ]);

        if ($validator->validate($post_data)) {
            $post_data['is_active'] = 0;
            $post_data['password'] = password_hash($post_data['password'], PASSWORD_DEFAULT);
            $post_data['token'] = User::getUniqueToken();

            if ($user = User::create($post_data)) {
                $mail = new Mail();
                $mail->addRecipient($user->email);
                $mail->setSubject('Activeer je account');
                $mail->setBody(u_view('_mail/user/activate', [
                    'user' => $user,
                ], null));
                $mail->send();
            }
        } else {
            return Response::jsonOrRedirect('/registreren', [
                'errors' => $validator->getErrors(),
            ], 422);
        }
    }
}
