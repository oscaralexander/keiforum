<?php

namespace App\Actions\User;

use App\Actions\Action;
use App\Models\User;
use Core\Auth;
use Core\Response;
use Core\View;

class OnlineAction extends Action
{
    protected $protected = true;

    public function run(): string
    {
        $user_ids = u_get('user_ids', $_GET, []);

        if (is_null($user_ids) || !count($user_ids) || !Auth::check()) {
            return Response::json([]);
        }

        $user_ids = array_filter($user_ids);
        $user_ids = array_unique($user_ids);
        $online_users = User::getOnlineUsers($user_ids);
        $online_user_ids = array_column($online_users, 'id');

        return Response::json([
            'online_user_ids' => $online_user_ids,
        ]);
    }
}
