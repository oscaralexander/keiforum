<?php

namespace App\Actions\Profile;

use App\Actions\Action;
use App\Models\User;
use Core\Request;
use Core\Response;
use Core\Validator;

class StoreAction extends Action
{
    public function run(): string
    {
        $post_data = [
            'name' => u_get('name', $_POST),
            'gender' => u_get('gender', $_POST),
            'gender_custom' => u_get('gender_custom', $_POST),
            'pronoun' => u_get('pronoun', $_POST),
            'pronoun_custom' => u_get('pronoun_custom', $_POST),
            'date_of_birth' => trim(get('dob_y', $_POST) . '-' . u_get('dob_m', $_POST) . '-' . u_get('dob_d', $_POST), '-'),
            'area_id' => u_get('area_id', $_POST),
        ];

        $validator = new Validator([
            'gender' => [
                '?in' => [
                    'arg' => ['m', 'v', 'custom'],
                    'error' => 'Onbekend geslacht geselecteerd.',
                ],
            ],
            'pronoun' => [
                '?in' => [
                    'arg' => ['hij/zijn', 'zij/haar', 'hen/hun', 'custom'],
                    'error' => 'Onbekend voornaamwoord geselecteerd.',
                ],
            ],
            'date_of_birth' => [
                '?date' => 'Wachtwoord is verplicht.',
            ],
            'area_id' => [
                '?exists' => [
                    'arg' => 'areas.id',
                    'error' => 'Onbekende wijk geselecteerd.',
                ],
            ],
        ]);

        if ($validator->validate($post_data)) {
            if (User::update($post_data, $current_user->id)) {
                return Response::redirect('/gebruiker/profiel', null, [
                    'message' => 'Opgeslagen!',
                    'message_type' => 'success',
                ]);
            }
        } else {
            return Response::jsonOrRedirect('/gebruiker/profiel', [
                'errors' => $validator->getErrors(),
            ], 422);
        }
    }
}
