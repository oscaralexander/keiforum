<?php

namespace App\Models;

use Core\DB;
use Core\Data;
use Core\Model;
use PDO;

class Thread extends Model
{
    const THREADS_PER_PAGE = 25;

    protected static $table = 'threads';

    public $id;
    public $forum_id;
    public $last_post_id;
    public $user_id;
    public $is_deleted;
    public $is_locked;
    public $is_sticky;
    public $posts_count;
    public $title;
    public $users_count;
    public $created_at;
    public $updated_at;

    public static function findByForumId(int $forum_id): array
    {
        $sth = DB::prepare('SELECT * FROM threads WHERE forum_id = :forum_id ORDER BY last_post_id DESC');
        $sth->execute(['forum_id' => $forum_id]);
        return $sth->fetchAll(PDO::FETCH_CLASS, get_called_class());
    }

    public function getUsers(): array
    {
        $sql = '
            SELECT
                user_id,
                COUNT(user_id) AS num_posts
            FROM posts
            WHERE
                thread_id = :thread_id
            GROUP BY user_id
            ORDER BY num_posts DESC';
        $sth = DB::prepare($sql);
        $sth->bindParam('thread_id', $this->id, PDO::PARAM_INT);
        $sth->execute();
        $results = $sth->fetchAll(PDO::FETCH_BOTH);
        $results = u_array_indexed($results, 'user_id');
        $user_ids = array_column($results, 'user_id');
        $users = User::findAll($user_ids);

        foreach ($users as &$user) {
            $user->num_posts = $results['user_id_' . $user->id]['num_posts'];
        }

        $users = u_array_indexed($users);
        return $users;
    }

    /**
     * Accessors
     */

    public function getUrlAttribute(): ?string
    {
        $forums = Data::get('forums', []);
        $forum_slug = $forums['id_' . $this->forum_id]->slug ?? '';
        return '/f/' . $forum_slug . '/' . u_slugify($this->title . '-' . $this->id);
    }
}
