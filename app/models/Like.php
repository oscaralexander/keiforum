<?php

namespace App\Models;

use Core\DB;
use Core\Model;

class Like extends Model
{
    protected static $table = 'likes';

    public $post_id;
    public $user_id;

    public static function all(): array
    {
        $sth = DB::query('SELECT * FROM ' . static::$table . ' ORDER BY post_id');
        return $sth->fetchAll(\PDO::FETCH_CLASS, __CLASS__);
    }
}
