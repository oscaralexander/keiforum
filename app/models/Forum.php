<?php

namespace App\Models;

use Core\DB;
use Core\Model;

class Forum extends Model
{
    protected static $table = 'forums';

    public $id;
    public $description;
    public $name;
    public $num_posts;
    public $num_threads;
    public $last_post_id;
    public $slug;
    public $sort_index;

    public static function all(): array
    {
        $sth = DB::query('SELECT * FROM ' . static::$table . ' ORDER BY sort_index');
        return $sth->fetchAll(\PDO::FETCH_CLASS, __CLASS__);
    }

    /**
     * Accessors
     */

    public function getUrlAttribute(): string
    {
        return '/f/' . $this->slug;
    }
}