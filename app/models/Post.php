<?php

namespace App\Models;

use App\Models\Like;
use Core\DB;
use Core\Model;

class Post extends Model
{
    const POSTS_PER_PAGE = 15;

    protected static $table = 'posts';

    public $id;
    public $thread_id;
    public $user_id;
    public $likes_count;
    public $text;
    public $created_at;
    public $deleted_at;
    public $updated_at;

    /**
     * Class methods
     */

    public static function findByThreadId(int $thread_id): array
    {
        $sth = DB::prepare('SELECT * FROM posts WHERE thread_id = :thread_id ORDER BY id ASC');
        $sth->execute(['thread_id' => $thread_id]);
        return $sth->fetchAll(\PDO::FETCH_CLASS, get_called_class());
    }

    /**
     * Instance methods
     */

    public function getLikes(): array
    {
        return Like::getWhere(['post_id' => $this->id]);
    }
}
