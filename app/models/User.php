<?php

namespace App\Models;

use Core\DB;
use Core\Data;
use Core\Model;
use PDO;

class User extends Model
{
    protected static $table = 'users';

    public $id;
    public $area_id;
    public $date_of_birth;
    public $email;
    public $gender;
    public $gender_custom;
    public $is_active;
    public $name;
    public $password;
    public $password_reset_token;
    public $pronoun;
    public $pronoun_custom;
    public $remember_token;
    public $token;
    public $username;
    public $created_at;
    public $last_active_at;
    public $updated_at;

    private $is_admin;
    private $is_moderator;

    /**
     * Class methods
     */

    public static function getUniquePasswordResetToken(): string
    {
        do {
            $password_reset_token = u_random_string(64);
        } while (static::exists(['password_reset_token' => $password_reset_token]));

        return $password_reset_token;
    }

    public static function getUniqueRememberToken(): string
    {
        do {
            $remember_token = u_random_string(64, true);
        } while (static::exists(['remember_token' => $remember_token]));

        return $remember_token;
    }

    public static function getUniqueToken(): string
    {
        do {
            $token = u_random_string(24);
        } while (static::exists(['token' => $token]));

        return $token;
    }

    /**
     * Instance methods
     */

    public static function getOnlineUsers(?array $user_ids = null): array
    {
        $bindings = [];

        for ($i = 0; $i < count($user_ids); $i++) {
            $bindings['in' . $i] = $user_ids[$i];
        }

        $in = ':' . implode(', :', array_keys($bindings));
        $bindings['offset'] = 600;

        $sql = '
            SELECT *
            FROM users
            WHERE
                id IN (' . $in . ')
                AND TIME_TO_SEC(TIMEDIFF(UTC_TIMESTAMP(), last_active_at)) < :offset
            ORDER BY id ASC';

        $sth = DB::prepare($sql);
        $sth->execute($bindings);
        $users = $sth->fetchAll(PDO::FETCH_CLASS, get_called_class());

        return $users;
    }

    public function isAdmin(): bool
    {
        if (isset($this->is_admin)) {
            return $this->is_admin;
        }

        $admins = Data::get('admins', []);
        $user_ids = array_column($admins, 'user_id');
        $this->is_admin = in_array($this->id, $user_ids);
        return $this->is_admin;
    }

    public function isModerator(): bool
    {
        if (isset($this->is_moderator)) {
            return $this->is_moderator;
        }

        $moderators = Data::get('moderators', []);
        $user_ids = array_column($moderators, 'user_id');
        $this->is_moderator = in_array($this->id, $user_ids);
        return $this->is_moderator;
    }

    public function updateLastActiveAt(): void
    {
        static::update([
            'last_active_at' => gmdate('Y-m-d H:i:s'),
        ], $this->id);
    }

    /**
     * Accessors
     */

    public function getActivationUrlAttribute(): string
    {
        return getenv('APP_URL') . '/gebruiker/activeren/' . $this->id . '/' . $this->token;
    }

    public function getAvatarUrlAttribute(): string
    {
        return u_gravatar($this->email);
    }

    public function getUrlAttribute(): string
    {
        return '/gebruiker/' . $this->username;
    }
}