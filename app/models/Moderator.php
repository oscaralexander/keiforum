<?php

namespace App\Models;

use Core\DB;
use Core\Model;

class Moderator extends Model
{
    protected static $table = 'moderators';

    public $user_id;

    public static function all(): array
    {
        $sth = DB::query('SELECT * FROM ' . static::$table . ' ORDER BY user_id');
        return $sth->fetchAll(\PDO::FETCH_CLASS, __CLASS__);
    }
}
