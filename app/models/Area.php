<?php

namespace App\Models;

use Core\DB;
use Core\Model;

class Area extends Model
{
    protected static $table = 'areas';

    public $id;
    public $name;
    public $slug;

    public static function all(): array
    {
        $sth = DB::query('SELECT * FROM ' . static::$table . ' ORDER BY name');
        return $sth->fetchAll(\PDO::FETCH_CLASS, __CLASS__);
    }

    /**
     * Accessors
     */

    public function getUrlAttribute(): string
    {
        return '/wijk/' . $this->slug;
    }
}