<?php
    $page_title = $forum->name;
?>
<div class="wrapper">
    <?php require APP_ROOT . '/views/_partials/header.php'; ?>
    <?php if ($current_user): ?>
        <div class="actions">
            <div class="actions__buttons">
                <a class="button button--primary" href="/nieuw-onderwerp?forum_id=<?= $forum->id; ?>">Nieuw onderwerp</a>
            </div>
        </div>
    <?php endif; ?>
    <div class="list" role="table">
        <header class="list__header" role="row">
            <div aria-sort="none" class="list__headerCol threadList__colTitle" role="columnheader">Onderwerp</div>
            <div aria-sort="none" class="list__headerCol threadList__colStat" role="columnheader">Reacties</div>
            <div aria-sort="none" class="list__headerCol threadList__colStat" role="columnheader">Leden</div>
            <div aria-sort="none" class="list__headerCol threadList__colLastReply" role="columnheader">Laatste reactie</div>
        </header>
        <ol class="list__body" role="rowgroup">
            <?php foreach ($threads as $thread): ?>
                <li class="list__row listItem" role="row">
                    <div class="list__col threadList__colTitle" role="cell">
                        <div class="listItem__avatarText">
                            <a class="avatar listItem__avatar" data-user-id="<?= $thread->user->id; ?>" href="<?= $thread->user->url; ?>"><img alt="<?= e($thread->user->name); ?>" class="avatar__img listItem__avatarImage" loading="lazy" src="<?= $thread->user->avatar_url; ?>"></a>
                            <div class="listItem__text">
                                <h4 class="listItem__title"><a href="<?= $thread->url; ?>"><?= e($thread->title); ?></a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="list__col threadList__colStat" role="cell">
                        <span class="listItem__stat"><?= $thread->posts_count; ?></span>
                    </div>
                    <div class="list__col threadList__colStat" role="cell">
                        <span class="listItem__stat"><?= $thread->users_count; ?></span>
                    </div>
                    <div class="list__col threadList__colLastReply" role="cell">
                        <?php if ($thread->last_post->user ?? null): ?>
                            <div class="listItem__avatarText">
                                <a class="avatar listItem__avatar" data-user-id="<?= $thread->last_post->user->id; ?>" href="<?= $thread->last_post->user->url; ?>"><img alt="<?= e($thread->last_post->user->name); ?>" class="avatar__img listItem__avatarImage" loading="lazy" src="<?= $thread->last_post->user->avatar_url; ?>"></a>
                                <div class="listItem__text">
                                    <div><a href="<?= $thread->last_post->user->url; ?>"><?= $thread->last_post->user->username; ?></a></div>
                                    <div><time datetime="<?= u_datetime_local($thread->last_post->created_at); ?>"><?= u_time_ago($thread->last_post->created_at); ?></time></div>
                                </div>
                            </div>
                            <a class="listItem__lastReplyLink" href="<?= $thread->url ?>/laatste" title="Ga naar laatste bericht"></a>
                        <?php else: ?>
                            &mdash;
                        <?php endif; ?>
                    </div>
                </li>
            <?php endforeach; ?>
        </ol>
    </div>
    <?php require APP_ROOT . '/views/_partials/pagination.php'; ?>
</div>
