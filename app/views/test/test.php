<?php
    $body_class = 'has-backdrop';
    $has_backdrop = true;
    $page_title = 'Editor';
?>
<div class="wrapper wrapper--small">
    <div class="panel">
        <form>
            <?php include APP_ROOT . '/views/_partials/editor.php'; ?>
        </form>
    </div>
</div>