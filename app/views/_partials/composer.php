<?php if ($current_user): ?>
    <form action="/bericht" class="composer js-xhrForm post" method="post">
        <div class="post__avatarColumn">
            <a class="avatar post__avatar js-avatar" data-avatar-user-id="<?= $current_user->id; ?>" href="<?= $current_user->url; ?>">
                <img alt="<?= $current_user->username; ?>" class="avatar__img post__avatarImg" loading="lazy" src="<?= $current_user->avatarUrl; ?>">
            </a>
        </div>
        <div class="post__main">
            <header class="post__header">
                <div class="post__user">
                    <?php if ($current_user): ?>
                        <div class="post__userUsername">
                            <?php if (empty($current_user->name)): ?>
                                <a class="post__userLink" href="<?= $current_user->url; ?>"><?= $current_user->username; ?></a>
                            <?php else: ?>
                                <a class="post__userLink" href="<?= $current_user->url; ?>"><?= $current_user->name; ?></a>
                                <span>(<?= $current_user->username; ?>)</span>
                            <?php endif; ?>
                            <?php if ($current_user->id === $thread->id): ?>
                                <i class="fas fa-star post__userIcon"></i>
                            <?php endif; ?>
                        </div>
                        <?php if ($current_user->area_id): ?>
                            <ul class="meta">
                                <li class="meta_item"><?= $areas['id_' . $current_user->area_id]->name; ?></li>
                            </ul>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </header>
            <div>
                <?php require APP_ROOT . '/views/_partials/editor.php'; ?>
            </div>
        </div>
    </form>
<?php else: ?>
    <div class="composer">
        <div class="composer__auth">
            <span class="composer__authIcon"><i class="fas fa-comments"></i></span>
            <p>
                <a href="/registreren">Registreer</a> of <a href="/inloggen">log in</a> om te reageren.
            </p>
        </div>
    </div>
<?php endif; ?>