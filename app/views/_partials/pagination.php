<?php if ($page && $num_pages): ?>
    <?php
        $base_url = preg_replace('/(\/\d+\/?)$/', '', $_SERVER['REQUEST_URI']);
        $window = 2;
        $show_pages = ($window * 2) + 1;

        if ($num_pages > $show_pages) {
            $p_min = $page - $window;
            $p_min = ($p_min < 1) ? 1 : $p_min;
            $p_max = $p_min + $show_pages - 1;

            if ($p_max > $num_pages) {
                $p_min = $p_min - ($p_max - $num_pages);
                $p_max = $num_pages;
            }
        } else {
            $p_min = 1;
            $p_max = $num_pages;
        }

        $range = range($p_min, $p_max, 1);
        $range_before = range(2, $p_min - 1);
        $range_after = range($p_max + 1, $num_pages - 1);
    ?>
    <form action="<?= $base_url; ?>" class="pagination js-pagination" method="get">
        <?php if ($page !== 1): ?>
            <a class="pagination__paginate pagination__paginate--prev" href="<?= $base_url . '/' . ($page === 1 ? 1 : $page - 1); ?>">&larr; Vorige</a>
        <?php endif; ?>
        <div class="pagination__pages">
            <?php if ($page > $window + 1): ?>
                <a class="pagination__page <?= ($page === 1) ? 'pagination__page--active' : ''; ?>" href="<?= $base_url; ?>">1</a>
            <?php endif; ?>
            <?php if ($page > $window + 2): ?>
                <span class="pagination__ellipsis">
                    &hellip;
                    <select class="js-paginationSelect" name="p_prev">
                        <option value="">&hellip;</option>
                        <?php foreach ($range_before as $p): ?>
                            <option value="<?= $p; ?>"><?= $p; ?></option>
                        <?php endforeach; ?>
                    </select>
                </span>
            <?php endif; ?>
            <?php foreach ($range as $p): ?>
                <?php
                    $p = intval($p);
                    $url = $base_url . (($p === 1) ? '' : '/' . $p);
                ?>
                <a class="pagination__page <?= ($p === $page) ? 'pagination__page--active' : ''; ?>" href="<?= $url; ?>"><?= $p; ?></a>
            <?php endforeach; ?>
            <?php if ($page < $num_pages - ($window + 1)): ?>
                <span class="pagination__ellipsis">
                    &hellip;
                    <select class="js-paginationSelect" name="p_next">
                        <option value="">&hellip;</option>
                        <?php foreach ($range_after as $p): ?>
                            <option value="<?= $p; ?>"><?= $p; ?></option>
                        <?php endforeach; ?>
                    </select>
                </span>
            <?php endif; ?>
            <?php if ($page < $num_pages - $window): ?>
                <a class="pagination__page <?= ($page === $num_pages) ? 'pagination__page--active' : ''; ?>" href="<?= $base_url . '/' . $num_pages; ?>"><?= $num_pages; ?></a>
            <?php endif; ?>
        </div>
        <?php if ($page !== $num_pages): ?>
            <a class="pagination__paginate pagination__paginate--next" href="<?= $base_url . '/' . ($page === $num_pages ? $num_pages : $page + 1); ?>">Volgende &rarr;</a>
        <?php endif; ?>
    </form>
<?php endif; ?>