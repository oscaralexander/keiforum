<div class="menu">
    <section class="menu__section">
        <ul class="menu__list">
            <li class="menu__listItem">
                <?php $active = $_SERVER['REQUEST_URI'] == '/'; ?>
                <a class="menu__link <?= $active ? 'menu__link--active' : ''; ?>" href="/">
                    <i class="fas fa-comment fa-fw"></i>
                    <span>Alle gesprekken</span>
                </a>
            </li>
            <li class="menu__listItem">
                <?php $active = u_starts_with('/leden', $_SERVER['REQUEST_URI']); ?>
                <a class="menu__link <?= $active ? 'menu__link--active' : ''; ?>" href="/leden">
                    <i class="fas fa-user fa-fw"></i>
                    <span>Leden</span>
                </a>
            </li>
            <li class="menu__listItem">
                <?php $active = u_starts_with('/agenda', $_SERVER['REQUEST_URI']); ?>
                <a class="menu__link <?= $active ? 'menu__link--active' : ''; ?>" href="/agenda">
                    <i class="fas fa-calendar-alt fa-fw"></i>
                    <span>Agenda</span>
                </a>
            </li>
        </ul>
    </section>
    <section class="menu__section">
        <h3 class="menu__heading">Forums</h3>
        <ul class="menu__list">
            <?php foreach ($forums as $forum): ?>
                <?php $active = u_starts_with('/f/' . $forum->slug, $_SERVER['REQUEST_URI']); ?>
                <li class="menu__listItem"><a class="menu__link <?= $active ? 'menu__link--active' : ''; ?>" href="<?= $forum->url; ?>"><?= $forum->name; ?></a></li>
            <?php endforeach; ?>
        </ul>
    </section>
    <section class="menu__section">
        <h3 class="menu__heading">Stadsdelen</h3>
        <ul class="menu__list">
            <?php foreach ($areas as $area): ?>
                <?php $active = u_starts_with('/w/' . $area->slug, $_SERVER['REQUEST_URI']); ?>
                <li class="menu__listItem"><a class="menu__link <?= $active ? 'menu__link--active' : ''; ?>" href="<?= $area->url; ?>"><?= $area->name; ?></a></li>
            <?php endforeach; ?>
        </ul>
    </section>
</div>