<header class="header">
    <ul class="path">
        <li class="path__item"><a class="path__link" href="/">Home</a></li>
        <?php if (isset($path) && is_array($path)): ?>
            <?php foreach ($path as $path_item): ?>
                <li class="path__item"><a class="path__link" href="<?= $path_item['href']; ?>"><?= $path_item['label']; ?></a></li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
    <h1 class="header__title"><?= e($page_title); ?></h1>
</header>