<!doctype html>
<html itemscope itemtype="http://schema.org/WebPage" lang="nl">
    <head>
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="initial-scale=1,maximum-scale=1,minimal-ui,shrink-to-fit=no,user-scalable=no,width=device-width" name="viewport">
        <meta content="<?= u_csrf_token(); ?>" name="csrf_token">
        <title><?= $page_title . ' - Keiforum Amersfoort' ?? 'Keiforum Amersfoort'; ?></title>
        <link href="https://fonts.googleapis.com" rel="preconnect">
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link as="script" href="/assets/js/app.js" rel="preload">
        <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,400;0,600;0,700;1,400;1,700&family=Source+Code+Pro&display=swap" rel="stylesheet">
        <?php if(!empty($has_editor)): ?>
            <link href="/assets/redactor/redactorx.min.css" rel="stylesheet" type="text/css">
        <?php endif; ?>
        <link href="/assets/css/app.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
    </head>
    <body class="<?= $body_class ?? ''; ?>">
        <nav class="nav">
            <a class="nav__logo" href="/" title="Keiforum">Keiforum</a>
            <form action="/zoeken" class="nav__search" method="GET">
                <input class="input input--rounded" name="q" type="text" placeholder="Zoeken">
            </form>
            <ul class="nav__user">
                <?php if (u_auth()): ?>
                    <li class="nav__userItem">
                        <form action="/uitloggen" method="post">
                            <button type="submit">Uitloggen</button>
                        </form>
                    </li>
                <?php else: ?>
                    <li class="nav__userItem"><a class="button" href="/inloggen">Inloggen</a></li>
                    <li class="nav__userItem"><a class="button" href="/registreren">Registreren</a></li>
                <?php endif; ?>
            </ul>
        </nav>
        <div class="page js-page">
            <div class="page__menu">
                <?php require APP_ROOT . '/views/_partials/menu.php'; ?>
            </div>
            <div class="page__body">
                <main class="page__main <?= isset($has_backdrop) ? 'page__main--backdrop' : ''; ?>">
                    <?= $content_for_layout; ?>
                    <?php if (isset($has_backdrop)): ?>
                        <a class="page__backdropCredit" href="http://aukehamers.nl/" target="_blank">
                            <i class="fa fa-camera"></i> Auke Hamers
                        </a>
                    <?php endif; ?>
                </main>
                <footer class="footer">
                    <div class="wrapper">
                        Copyright &copy; <?= date('Y'); ?>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.23.0/axios.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.21/lodash.min.js"></script>
        <?php if(!empty($has_editor)): ?>
            <script src="/assets/redactor/redactorx.js"></script>
            <script src="/assets/redactor/nl.js"></script>
        <?php endif; ?>
        <script src="/assets/js/app.js"></script>
    </body>
</html>