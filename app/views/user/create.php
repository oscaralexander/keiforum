<?php
    $body_class = 'has-backdrop';
    $page_title = 'Registreren';
?>
<div class="wrapper wrapper--small">
    <div class="panel">
        <?php include APP_ROOT . '/views/_partials/header.php'; ?>
        <form action="/registreren" class="js-xhrForm" method="post" spellcheck="false">
            <input name="csrf_token" type="hidden" value="<?= u_csrf_token(); ?>">
            <fieldset class="fieldset">

                <?php $err = u_error('email'); ?>
                <div class="formField js-xhrFormField <?= empty($err) ? '' : 'has-error'; ?>">
                    <label class="formField__label" for="inputEmail">E-mailadres</label>
                    <input autocomplete="email" class="input input--large js-input" id="inputEmail" name="email" required type="email" value="<?= u_old('email'); ?>">
                    <p class="formField__error js-xhrFormFieldError"><?= e($err); ?></p>
                </div>

                <?php $err = u_error('username'); ?>
                <div class="formField js-xhrFormField <?= empty($err) ? '' : 'has-error'; ?>">
                    <label class="formField__label" for="inputUsername">Gebruikersnaam</label>
                    <input autocomplete="username" class="input input--large js-input" id="inputUsername" name="username" required type="text" value="<?= u_old('username'); ?>">
                    <p class="formField__error js-xhrFormFieldError"><?= e($err); ?></p>
                </div>

                <?php $err = u_error('password'); ?>
                <div class="formField js-xhrFormField <?= empty($err) ? '' : 'has-error'; ?>">
                    <label class="formField__label" for="inputPassword">Wachtwoord</label>
                    <div class="js-password password">
                        <input autocomplete="off" class="input input--large js-input" id="inputPassword" name="password" required type="password" value="<?= u_old('password'); ?>">
                        <button class="password__toggle js-passwordToggle" type="button"></button>
                    </div>
                    <p class="formField__error js-xhrFormFieldError"><?= e($err); ?></p>
                    <ul class="js-passwordRequirements passwordRequirements" data-password-requirements-digits="<?= getenv('APP_PASSWORD_REQUIREMENTS_DIGITS'); ?>" data-password-requirements-input="inputPassword" data-password-requirements-length="<?= getenv('APP_PASSWORD_REQUIREMENTS_LENGTH'); ?>">
                        <li class="js-passwordRequirementsLength passwordRequirements__requirement">Minimaal <?= getenv('APP_PASSWORD_REQUIREMENTS_LENGTH'); ?> tekens lang</li>
                        <li class="js-passwordRequirementsDigits passwordRequirements__requirement">Minimaal <?= getenv('APP_PASSWORD_REQUIREMENTS_DIGITS'); ?> cijfers</li>
                        <li class="js-passwordRequirementsMixedCase passwordRequirements__requirement">Hoofdletters én kleine letters</li>
                    </ul>
                </div>

                <?php $err = u_error('terms'); ?>
                <div class="formField js-xhrFormField <?= empty($err) ? '' : 'has-error'; ?>">
                    <div class="inputCheckbox">
                        <input name="terms" type="hidden" value="0">
                        <input class="inputCheckbox__input" id="inputTerms" name="terms" type="checkbox" value="1">
                        <label class="inputCheckbox__label" for="inputTerms">Ik ga akkoord met de <a href="/voorwaarden">algemene voorwaarden</a></label>
                    </div>
                    <p class="formField__error js-xhrFormFieldError"><?= e($err); ?></p>
                </div>
            </fieldset>
            <div class="buttons">
                <button class="button button--primary" type="submit">Registreren</button>
                <a class="button" href="/">Annuleren</a>
            </div>
        </form>
    </div>
</div>
