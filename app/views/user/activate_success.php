<?php
    $page_title = 'Welkom bij Keiforum!';
?>
<div class="wrapper wrapper--medium">
    <div class="panel">
        <?php include APP_ROOT . '/views/_partials/header.php'; ?>
        <p class="u-m-xl u-text-body">
            Keiforum is een plek voor échte mensen.
            Daarom stellen we het op prijs als je iets meer over jezelf wilt vertellen.
            Deze informatie is alleen zichtbaar voor geregistreerde leden.
        </p>
        <?php include APP_ROOT . '/views/profile/_form.php'; ?>
    </div>
</div>