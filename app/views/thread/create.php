<?php
    $has_editor = u_auth();
    $page_title = 'Nieuw onderwerp';
?>
<div class="wrapper wrapper--medium">
    <div class="panel">
        <?php require APP_ROOT . '/views/_partials/header.php'; ?>
        <form action="/nieuw-onderwerp" class="js-xhrForm" method="post">
            <input name="csrf_token" type="hidden" value="<?= u_csrf_token(); ?>">
            <fieldset class="fieldset">
                <div class="grid grid--gap-l">
                    <div class="grid__col m:grid__col--1/2">
                        <?php $err = u_error('forum_id'); ?>
                        <div class="formField <?= empty($err) ? '' : 'is-error'; ?>">
                            <label class="formField__label" for="inputForumId">Forum</label>
                            <select class="js-inputSelect" id="inputForumId" name="forum_id">
                                <?php foreach ($forums as $forum): ?>
                                    <?php $selected = ($forum->id == $forum_id) ? 'selected' : ''; ?>
                                    <option <?= $selected; ?> value="<?= $forum->id; ?>"><?= e($forum->name); ?></option>    
                                <?php endforeach; ?>
                            </select>
                            <p class="formField__error"><?= e($err); ?></p>
                        </div>
                    </div>
                    <div class="grid__col m:grid__col--1/2">
                        <?php $err = u_error('area_id'); ?>
                        <div class="formField <?= empty($err) ? '' : 'is-error'; ?>">
                            <label class="formField__label" for="inputAreaId">Wijk (optioneel)</label>
                            <select class="js-inputSelect" id="inputAreaId" name="area_id">
                                <option value="0">Geen</option>
                                <option disabled value="0">&mdash;</option>
                                <?php foreach ($areas as $area): ?>
                                    <?php $selected = ($area->id == $area_id) ? 'selected' : ''; ?>
                                    <option <?= $selected; ?> value="<?= $area->id; ?>"><?= e($area->name); ?></option>    
                                <?php endforeach; ?>
                            </select>
                            <p class="formField__error"><?= e($err); ?></p>
                        </div>
                    </div>
                </div>

                <?php $err = u_error('title'); ?>
                <div class="formField js-xhrFormField <?= empty($err) ? '' : 'has-error'; ?>">
                    <label class="formField__label" for="inputTitle">Titel</label>
                    <input autocomplete="off" class="input input--large js-input" id="inputTitle" name="title" required type="title" value="<?= u_old('title'); ?>">
                    <p class="formField__error js-xhrFormFieldError"><?= e($err); ?></p>
                </div>

                <div class="editor js-editor u-m-xl" data-editor-id="0">
                    <div class="editor__redactor">
                        <textarea id="editor-0" name="text"><?= u_old('text', '<p></p>'); ?></textarea>
                    </div>
                </div>

                <div class="buttons">
                    <button class="button button--primary" type="submit">Plaatsen</button>
                    <a class="button" href="/">Annuleren</a>
                </div>
            </fieldset>
        </form>
    </div>
</div>
