<?php
    $counter = 0;
    $has_editor = u_auth();
    $page_title = $thread->title;
?>
<div class="wrapper">
    <?php require APP_ROOT . '/views/_partials/header.php'; ?>
    <div class="postList">
        <?php foreach ($posts as $post): ?>
            <?php
                $counter++;
                $is_liked = in_array($post->id, $user_likes);
                $post_num = ($posts_per_page * ($page - 1)) + $counter;
                $user = $post->user;
            ?>
            <article class="post js-post" data-post-id="<?= $post->id; ?>" id="post-<?= $post_num; ?>">
                <div class="post__avatarColumn">
                    <a class="avatar post__avatar js-avatar" data-user-id="<?= $user->id; ?>" href="<?= $user->url; ?>" title="<?= $user->username; ?>">
                        <img alt="<?= $user->username; ?>" class="avatar__img post__avatarImg" loading="lazy" src="<?= $user->avatarUrl; ?>">
                    </a>
                </div>
                <div class="post__main">
                    <header class="post__header">
                        <div class="post__user">
                            <div class="post__userName">
                                <a class="post__userLink" href="<?= $user->url; ?>"><?= $user->username; ?></a>
                                <?php if ($user->id === $thread->user_id): ?>
                                    <span class="post__userLabel post__userLabel--starter" title="Gesprekstarter">GS</span>
                                <?php endif; ?>
                                <?php if ($user->isAdmin()): ?>
                                    <span class="post__userLabel post__userLabel--admin">Beheerder</span>
                                <?php elseif ($user->isModerator()): ?>
                                    <span class="post__userLabel post__userLabel--moderator">Moderator</span>
                                <?php endif; ?>
                            </div>
                            <?php if ($current_user): ?>
                                <ul class="meta">
                                    <?php if (!empty($user->name)): ?>
                                        <li class="meta_item"><?= e($user->name); ?></li>
                                    <?php endif; ?>
                                    <?php if (!empty($user->area_id)): ?>
                                        <li class="meta_item"><?= e($areas['id_' . $user->area_id]->name); ?></li>
                                    <?php endif; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                        <ul class="meta">
                            <li class="meta__item"><time class="post__timestamp" datetime="<?= u_datetime_local($post->created_at); ?>" title="<?= u_datetime_local($post->created_at); ?>"><?= u_time_ago($post->created_at); ?></time></li>
                            <li class="meta__item"><a class="post__anchor" href="<?= $thread->url; ?>?post=<?= $post->id; ?>">#<?= $post_num; ?></a></li>
                        </ul>
                    </header>
                    <div>
                        <main class="formatted post__body">
                            <?= $post->text; ?>
                        </main>
                        <?php if ($current_user): ?>
                            <footer class="post__footer">
                                <ul class="post__actions">
                                    <?php if ($user->id === $current_user->id || $current_user->isAdmin()): ?>
                                        <li class="post__action"><a href="#"><i class="fas fa-pen"></i> Bewerken</a></li>
                                        <li class="post__action"><a href="#"><i class="fas fa-trash"></i> Verwijderen</a></li>
                                    <?php else: ?>
                                        <li class="post__action"><a href="#"><i class="fas fa-reply"></i> Reageren</a></li>
                                        <li class="post__action"><a href="#"><i class="far fa-frown"></i> Melden</a></li>
                                    <?php endif; ?>
                                </ul>
                                <form action="/like" class="likes <?= $is_liked ? 'is-liked' : ''; ?> js-likes js-xhrForm" data-likes-count="<?= $post->likes_count; ?>" method="post">
                                    <input name="csrf_token" type="hidden" value="<?= u_csrf_token(); ?>">
                                    <input name="post_id" type="hidden" value="<?= $post->id; ?>">
                                    <div class="likes__count js-likesCount"><?= $post->likes_count; ?></div>
                                    <button class="likes__like" type="submit">Vind ik leuk</button>
                                </form>
                            </footer>
                        <?php endif; ?>
                    </div>
                </div>
            </article>
        <?php endforeach; ?>
    </div>
    <?php require APP_ROOT . '/views/_partials/pagination.php'; ?>
    <?php require APP_ROOT . '/views/_partials/composer.php'; ?>
    <h3 class="u-m-l">In dit gesprek</h3>
    <ol class="participantList">
        <?php foreach ($users as $user): ?>
            <li class="participantListItem">
                <div class="participantListItem__avatarUser">
                    <a href="<?= $user->url; ?>"><img alt="" class="participantListItem__avatar" loading="lazy" src="<?= $user->avatarUrl; ?>"></a>
                    <div>
                        <div class="participantListItem__username">
                            <a href="<?= $user->url; ?>"><?= $user->username; ?></a>
                            <?php if ($user->id === $thread->id): ?>
                                <i class="fas fa-star"></i>
                            <?php endif; ?>
                        </div>
                        <?php if (u_auth()): ?>
                            <div class="participantListItem__name"><?= $user->name; ?></div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="participantListItem__numPosts"><?= $user->num_posts; ?></div>
            </li>
        <?php endforeach; ?>
    </ol>
</div>