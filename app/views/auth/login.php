<?php
    $body_class = 'has-backdrop';
    $has_backdrop = true;
    $page_title = 'Inloggen';
?>
<div class="wrapper wrapper--small">
    <div class="panel">
        <?php include APP_ROOT . '/views/_partials/header.php'; ?>
        <form action="/inloggen" class="js-xhrForm" method="post" spellcheck="false">
            <input name="csrf_token" type="hidden" value="<?= u_csrf_token(); ?>">
            <fieldset class="fieldset">
                <?php $err = u_error('username_or_email'); ?>
                <div class="formField js-xhrFormField <?= empty($err) ? '' : 'is-error'; ?>">
                    <label class="formField__label" for="inputEmail">Gebruikersnaam of e-mailadres</label>
                    <input autocomplete="username" class="input input--large js-input" id="inputEmail" name="username_or_email" required value="<?= u_old('username_or_email'); ?>">
                    <p class="formField__error js-xhrFormFieldError"><?= e($err); ?></p>
                </div>
                <?php $err = u_error('password'); ?>
                <div class="formField js-xhrFormField <?= empty($err) ? '' : 'is-error'; ?>">
                    <label class="formField__label" for="inputPassword">
                        <span>Wachtwoord</span>
                        <a href="/wachtwoord-vergeten">Vergeten?</a>
                    </label>
                    <div class="js-password password">
                        <input autocomplete="current-password" class="input input--large js-input" id="inputPassword" name="password" required type="password" value="<?= u_old('password'); ?>">
                        <button class="password__toggle js-passwordToggle" type="button"></button>
                    </div>
                    <p class="formField__error js-xhrFormFieldError"><?= e($err); ?></p>
                </div>
                <?php $err = u_error('terms'); ?>
                <div class="formField js-xhrFormField <?= empty($err) ? '' : 'is-error'; ?>">
                    <div class="inputCheckbox">
                        <input name="remember" type="hidden" value="0">
                        <input class="inputCheckbox__input" id="inputRemember" name="remember" type="checkbox" value="1">
                        <label class="inputCheckbox__label" for="inputRemember">Onthoud mij</label>
                    </div>
                    <p class="formField__error js-xhrFormFieldError"><?= e($err); ?></p>
                </div>
            </fieldset>
            <div class="buttons">
                <button class="button button--primary" type="submit">Inloggen</button>
                <a class="button" href="/">Annuleren</a>
            </div>
        </form>
    </div>
</div>