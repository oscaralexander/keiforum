<?php
    $body_class = 'has-backdrop';
    $has_backdrop = true;
    $page_title = 'Wachtwoord gewijzigd';
?>
<div class="wrapper wrapper--small">
    <div class="panel">
        <?php include APP_ROOT . '/views/_partials/header.php'; ?>
        <p class="u-m-xl u-text-body">
            We hebben je voor het gemak meteen even ingelogd met ‘onthoud mij’-cookie, dus log straks even uit als je achter een publieke computer zit.
        </p>
        <div class="buttons">
            <a class="button button--primary" href="/">Naar de homepage</a>
        </div>
    </div>
</div>