<?php
    $body_class = 'has-backdrop';
    $has_backdrop = true;
    $page_title = 'Wachtwoord vergeten?';
?>
<div class="wrapper wrapper--small">
    <div class="panel">
        <?php include APP_ROOT . '/views/_partials/header.php'; ?>
        <p class="u-m-xl u-text-body">
            Kan gebeuren!
            We sturen je een link om je wachtwoord te veranderen.
        </p>
        <form action="/wachtwoord-vergeten" autocomplete="off" class="js-xhrForm <?= u_old('success') ? 'is-success' : ''; ?>" method="post" spellcheck="false">
            <input name="csrf_token" type="hidden" value="<?= u_csrf_token(); ?>">
            <div class="formMessage formMessage--success">
                Gelukt!
                Als er een account is gevonden met deze gegevens, dan is er nu een mail onderweg.
            </div>
            <fieldset class="fieldset">
                <?php $err = u_error('username_or_email'); ?>
                <div class="formField js-xhrFormField <?= empty($err) ? '' : 'is-error'; ?>">
                    <label class="formField__label" for="inputUsernameOrEmail">Gebruikersnaam of e-mailadres</label>
                    <input autocomplete="off" autofocus class="input input--large js-input" id="inputUsernameOrEmail" name="username_or_email" required value="<?= u_old('username_or_email'); ?>">
                    <p class="formField__error js-xhrFormFieldError"><?= e($err); ?></p>
                </div>
            </fieldset>
            <div class="buttons">
                <button class="button button--primary" type="submit">Versturen</button>
                <a class="button" href="/inloggen">Annuleren</a>
            </div>
        </form>
    </div>
</div>