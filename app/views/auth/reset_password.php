<?php
    $body_class = 'has-backdrop';
    $has_backdrop = true;
    $page_title = 'Wachtwoord wijzigen';
?>
<div class="wrapper wrapper--small">
    <div class="panel">
        <?php include APP_ROOT . '/views/_partials/header.php'; ?>
        <form action="/wachtwoord-wijzigen" class="js-xhrForm" method="post" spellcheck="false">
            <div class="formMessage js-xhrFormMessage"></div>
            <input name="csrf_token" type="hidden" value="<?= u_csrf_token(); ?>">
            <input name="token" type="hidden" value="<?= u_old('token', $token); ?>">
            <fieldset class="fieldset">
                <!-- password -->
                <?php $err = u_error('password'); ?>
                <div class="formField js-xhrFormField <?= empty($err) ? '' : 'is-error'; ?>">
                    <label class="formField__label" for="inputPassword">Nieuw wachtwoord</label>
                    <div class="js-password password">
                        <input autocomplete="off" class="input input--large js-input" id="inputPassword" name="password" required type="password" value="<?= u_old('password'); ?>">
                        <button class="password__toggle js-passwordToggle" type="button"></button>
                    </div>
                    <p class="formField__error js-xhrFormFieldError"><?= e($err); ?></p>
                    <ul class="js-passwordRequirements passwordRequirements" data-password-requirements-digits="<?= getenv('APP_PASSWORD_REQUIREMENTS_DIGITS'); ?>" data-password-requirements-input="inputPassword" data-password-requirements-length="<?= getenv('APP_PASSWORD_REQUIREMENTS_LENGTH'); ?>">
                        <li class="js-passwordRequirementsLength passwordRequirements__requirement">Minimaal <?= getenv('APP_PASSWORD_REQUIREMENTS_LENGTH'); ?> tekens lang</li>
                        <li class="js-passwordRequirementsDigits passwordRequirements__requirement">Minimaal <?= getenv('APP_PASSWORD_REQUIREMENTS_DIGITS'); ?> cijfers</li>
                        <li class="js-passwordRequirementsMixedCase passwordRequirements__requirement">Hoofdletters én kleine letters</li>
                    </ul>
                </div>
                <!-- password_confirm -->
                <?php $err = u_error('password_confirm'); ?>
                <div class="formField js-xhrFormField <?= empty($err) ? '' : 'is-error'; ?>">
                    <label class="formField__label" for="inputPasswordConfirm">Herhaal nieuw wachtwoord</label>
                    <div class="js-password password">
                        <input autocomplete="off" class="input input--large js-input" id="inputPasswordConfirm" name="password_confirm" required type="password" value="">
                        <button class="password__toggle js-passwordToggle" type="button"></button>
                    </div>
                    <p class="formField__error js-xhrFormFieldError"><?= e($err); ?></p>
                </div>
            </fieldset>
            <div class="buttons">
                <button class="button button--primary" type="submit">Opslaan</button>
            </div>
        </form>
    </div>
</div>