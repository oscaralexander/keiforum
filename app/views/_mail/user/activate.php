<html>
    <head>
        <meta charset="utf-8">
        <title>Email test</title>
    </head>
    <body>
        <h1>Welkom bij Keiforum!</h1>
        <p>
            Klik op de onderstaande link om je account te activeren:
        </p>
        <p>
            <a href="<?= $user->activation_url; ?>">Activeer mijn account</a>
        </p>
        <p>
            Tot ziens op het forum!
        </p>
    </body>
</html>
