<html>
    <head>
        <meta charset="utf-8">
        <title>Keirforum</title>
    </head>
    <body>
        <h1>Nieuw wachtwoord instellen</h1>
        <p>
            Klik op de onderstaande link om je wachtwoord te wijzigen:
        </p>
        <p>
            <a href="<?= getenv('APP_URL'); ?>/wachtwoord-wijzigen/<?= $user->password_reset_token; ?>">Wachtwoord wijzigen</a>
        </p>
        <p>
            Tot ziens op het forum!
        </p>
    </body>
</html>
