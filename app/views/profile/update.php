<?php

use App\Models\User;
use Core\DB;
use Core\Response;
use Core\Validator;

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    error(403);
}

$data = [
    'username' => get('username', $_POST),
    'email' => get('email', $_POST),
    'password' => get('password', $_POST),
    'name' => get('name', $_POST),
    'gender' => get('gender', $_POST),
    'pronoun' => get('pronoun', $_POST),
    'dob' => get('dob_y', $_POST) . '-' . get('dob_m', $_POST) . '-' . get('dob_d', $_POST),
    'area_id' => get('area_id', $_POST),
    'terms' => get('terms', $_POST),
];

$data['dob'] = trim($data['dob'], '-');
$allowed_genders = ['m', 'v', 'custom'];
$allowed_pronouns = ['hij/zijn', 'zij/haar', 'hen/hun', 'custom'];

$rules = [
    'username' => [
        'required' => 'Gebruikersnaam is verplicht.',
        'unique' => [
            'arg' => 'users',
            'error' => 'Deze gebruikersnaam is helaas al bezet.',
        ],
    ],
    'email' => [
        'required' => 'E-mailadres is verplicht.',
        'email' => 'Ongeldig e-mailadres.',
        'unique' => [
            'arg' => 'users',
            'error' => 'Dit e-mailadres heeft al een account.',
        ],
    ],
    'password' => [
        'required' => 'Wachtwoord is verplicht.',
    ],
    'name' => [
        'required' => 'Naam is verplicht.',
    ],
    'gender' => [
        '?in' => [
            'arg' => $allowed_genders,
            'error' => 'Ongeldig geslacht geselecteerd.',
        ],
    ],
    'pronoun' => [
        '?in' => [
            'arg' => $allowed_pronouns,
            'error' => 'Ongeldig voornaamwoord geselecteerd.',
        ],
    ],
    'dob' => [
        '?date' => 'Ongeldige geboortedatum geselecteerd.',
    ],
    'area_id' => [
        '?exists' => [
            'arg' => 'areas.id',
            'error' => 'Ongeldige wijk geselecteerd.',
        ],
    ],
    'terms' => [
        'accepted' => 'De algemene voorwaarden moeten geaccepteerd worden.',
    ],
];

$validator = new Validator($rules);

print_json($data);

if ($validator->validate($data)) {
    $data['login_token'] = User::getUniqueLoginToken();
    $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
    $user = User::create($data);

    print_json($user);
} else {
    $errors = $validator->getErrors();

    if (Request::isAjax()) {
        Response::json($errors, 422);
    } else {
        Response::redirect('/registreren', null, $errors);
    }
}