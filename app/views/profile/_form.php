<form action="/profiel/<?= $current_user->id; ?>" method="post" spellcheck="false">
    <input name="csrf_token" type="hidden" value="<?= u_csrf_token(); ?>">
    <fieldset class="fieldset">
        <div class="formField">
            <label class="formField__label">Profielfoto</label>
            <div class="uploader js-uploader" data-uploader-allowed-types="image/gif,image/jpeg,image/png" data-uploader-max-files="1">
                <ul class="uploader__uploads js-uploaderUploads"></ul>
                <div class="uploader__form">
                    <button class="button button--square" type="button">
                        Bladeren&hellip;
                        <input accept=".jpeg,.jpg,.png" name="file" type="file">
                    </button>
                    <p class="uploader__allowedTypes">Alleen JPG of PNG bestanden zijn toegestaan. Maximaal 5MB.</p>
                </div>
                <script class="js-uploaderUploadTemplate" type="text/template">
                    <li class="upload js-upload">
                        <div class="upload__preview js-uploadPreview"></div>
                        <div class="upload__details">
                            <div class="upload__file">
                                <div class="upload__name js-uploadName"><%- fileName %></div>
                                <button class="upload__cancel js-uploadCancel" title="Annuleren" type="button">&times;</button>
                            </div>
                            <div class="upload__progress">
                                <div class="upload__progressFill js-uploadProgressFill"></div>
                            </div>
                        </div>
                        <input class="js-uploadInput" name="upload_file[]" type="hidden">
                    </li>
                </script>
            </div>
        </div>

        <?php $err = u_error('name'); ?>
        <div class="formField <?= empty($err) ? '' : 'is-error'; ?>">
            <label class="formField__label" for="inputName">Naam</label>
            <input class="input" id="inputName" name="name" placeholder="Naam" type="text" value="<?= u_old('name'); ?>">
            <p class="formField__error"><?= e($err); ?></p>
        </div>
        <div class="grid grid--gap-m">
            <div class="grid__col m:grid__col--1/2">
                <?php
                    $err = u_error('gender');
                    $genders = [
                        'm' => 'Man',
                        'v' => 'Vrouw',
                        'custom' => 'Anders...',
                    ];
                ?>
                <div class="formField <?= empty($err) ? '' : 'is-error'; ?>">
                    <label class="formField__label" for="inputGender">Geslacht</label>
                    <select class="js-inputSelect" id="inputGender" name="gender">
                        <option value="">&mdash;</option>
                        <?php foreach ($genders as $value => $label): ?>
                            <option value="<?= $value; ?>"><?= $label; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <p class="formField__error"><?= e($err); ?></p>
                </div>
            </div>
            <div class="grid__col m:grid__col--1/2">
                <?php
                    $err = u_error('pronoun');
                    $pronouns = [
                        'hij/zijn' => 'Hij / Zijn',
                        'zij/haar' => 'Zij / Haar',
                        'hen/hun' => 'Hen / Hun',
                        'custom' => 'Anders...',
                    ];
                ?>
                <div class="formField <?= empty($err) ? '' : 'is-error'; ?>">
                    <label class="formField__label" for="inputPronoun">Voornaamwoord</label>
                    <select class="js-inputSelect" id="pronoun" name="pronoun">
                        <option value="">&mdash;</option>
                        <?php foreach ($pronouns as $value => $label): ?>
                            <option value="<?= $value; ?>"><?= $label; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <p class="formField__error"><?= e($err); ?></p>
                </div>
            </div>
        </div>
        <?php $err = u_error('dob'); ?>
        <div class="formField <?= empty($err) ? '' : 'is-error'; ?>">
            <label class="formField__label">Geboortedatum</label>
            <div class="inputDate">
                <select class="js-inputSelect" name="dob_d">
                    <option value="">&mdash;</option>
                    <?php for ($d = 1; $d <= 31; $d++): ?>
                        <?php
                            $value = str_pad($d, 2, '0', STR_PAD_LEFT);
                        ?>
                        <option value="<?= $value; ?>"><?= $d; ?></option>
                    <?php endfor; ?>
                </select>
                <select class="js-inputSelect" name="dob_m">
                    <option value="">&mdash;</option>
                    <?php for ($m = 1; $m <= 12; $m++): ?>
                        <?php
                            $date = new DateTime();
                            $date->setDate(date('Y'), $m, date('d'));
                            $value = str_pad($m, 2, '0', STR_PAD_LEFT);
                        ?>
                        <option value="<?= $value; ?>"><?= ucfirst(strftime('%B', $date->getTimestamp())); ?></option>
                    <?php endfor; ?>
                </select>
                <select class="js-inputSelect" name="dob_y">
                    <option value="">&mdash;</option>
                    <?php for ($y = (int) date('Y'); $y >= date('Y') - 120; $y--): ?>
                        <option value="<?= $y; ?>"><?= $y; ?></option>
                    <?php endfor; ?>
                </select>
            </div>
            <p class="formField__error"><?= e($err); ?></p>
        </div>
        <?php $err = u_error('area_id'); ?>
        <div class="formField <?= empty($err) ? '' : 'is-error'; ?>">
            <label class="formField__label" for="inputAreaId">Wijk</label>
            <select class="js-inputSelect" id="inputAreaId" name="area_id">
                <option value="">&mdash;</option>
                <?php foreach ($areas as $area): ?>
                    <option value="<?= $area->id; ?>"><?= e($area->name); ?></option>    
                <?php endforeach; ?>
            </select>
            <p class="formField__error"><?= e($err); ?></p>
        </div>
    </fieldset>
    <button class="button button--primary" type="submit">Opslaan</button>
</form>