<?php
    $page_title = 'Profiel';
?>
<div class="wrapper wrapper--narrow">
    <div class="panel">
        <?php include APP_ROOT . '/views/_common/header.php'; ?>
        <?php include APP_ROOT . '/views/profile/_form.php'; ?>
    </div>
</div>