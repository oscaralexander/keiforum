<?php

return $routes = [
    'Area' => [
        'GET:w/(?<slug>[^/]+)(/(?<page>\d+))' => 'ShowAction',
    ],

    'Auth' => [
        'GET:wachtwoord-gewijzigd' => 'ResetPasswordSuccessAction',
        'GET:wachtwoord-vergeten' => 'ForgotPasswordAction',
        'POST:wachtwoord-vergeten' => 'ForgotPasswordPostAction',
        'GET:wachtwoord-wijzigen/(?<token>[^/]+)' => 'ResetPasswordAction',
        'POST:wachtwoord-wijzigen' => 'ResetPasswordPostAction',
        'GET:inloggen' => 'LoginAction',
        'POST:inloggen' => 'LoginPostAction',
        'POST:uitloggen' => 'LogoutPostAction',
    ],

    'Forum' => [
        'GET:f/(?<slug>[^/]+)(/(?<page>\d+))?' => 'ShowAction',
    ],

    'Index' => [
        'GET:' => 'ShowAction',
    ],

    'Like' => [
        'POST:like' => 'ToggleAction',
    ],

    'Profile' => [
        'GET:profiel' => 'EditAction',
        'POST:profiel' => 'StoreAction',
    ],

    'Thread' => [
        'GET:nieuw-onderwerp' => 'CreateAction',
        'GET:f/(?<forum_slug>[^/]+)/(?<slug>[^/]+)-(?<id>\d+)(/(?<page>\d+))?' => 'ShowAction',
        'POST:nieuw-onderwerp' => 'StoreAction',
    ],

    'Test' => [
        'GET:test' => 'TestAction',
    ],

    'User' => [
        'GET:gebruiker/activeren/(?<id>\d+)/(?<token>[^/]+)' => 'ActivateAction',
        'GET:online' => 'OnlineAction',
        'GET:registreren' => 'CreateAction',
        'POST:registreren' => 'StoreAction',
    ],
];
