<?php

namespace Core;

class Request
{
    public static $csrf_token = null;
    protected static $errors = [];
    protected static $input_old = [];
    protected static $flash = [];

    private function __construct() {}

    public static function getCsrfToken(): ?string
    {
        return u_get('csrf_token', $_SESSION);
    }

    public static function getError(string $name, $default = null): mixed
    {
        $errors = u_get('errors.' . $name, static::$flash);

        if (is_array($errors) && count($errors)) {
            return $errors[0];
        }

        return $default;
    }

    public static function getErrors(string $name = null): array
    {
        $key = 'errors' . ($name ? '.' . $name : '');
        return u_get($key, static::$flash) ?? [];
    }

    public static function getFlash(string $name = null, mixed $default = null): ?string
    {
        return is_null($name) ? static::$flash : u_get($name, static::$flash, $default);
    }

    public static function getInputOld(string $name = null, mixed $default = null): ?string
    {
        return is_null($name) ? static::$input_old : u_get($name, static::$input_old, $default);
    }

    public static function getMethod(): string
    {
        return strtoupper($_SERVER['REQUEST_METHOD']);
    }

    public static function getURI(): string
    {
        return $_SERVER['REQUEST_URI'];
    }

    public static function init(): void
    {
        static::$csrf_token = u_get('csrf_token', $_SESSION);
        static::$flash = u_get('flash', $_SESSION, []);
        static::$input_old = u_get('input_old', $_SESSION, []);
        static::setCsrfToken();

        unset($_SESSION['flash']);
        unset($_SESSION['input_old']);

        $_SESSION['input_old'] = $_REQUEST;
    }

    public static function isAjax(): bool
    {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
    }

    public static function isPost(): bool
    {
        return (strtolower($_SERVER['REQUEST_METHOD']) === 'post');
    }

    public static function setCsrfToken(): void
    {
        $csrf_token = bin2hex(random_bytes(32));
        header('X-CSRF-Token: ' . $csrf_token);
        $_SESSION['csrf_token'] = $csrf_token;
    }

    public static function setFlash(array $flash): void
    {
        static::$flash = $flash;
        $_SESSION['flash'] = $flash;
    }

    public static function validateCsrfToken(): bool
    {
        $csrf_token_header = u_get('HTTP_X_CSRF_TOKEN', $_SERVER);
        $csrf_token = u_get('csrf_token', $_POST, $csrf_token_header);

        return hash_equals(static::$csrf_token, $csrf_token);
    }
}
