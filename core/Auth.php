<?php

namespace Core;

use App\Models\User;

class Auth
{
    protected static $user = null;

    public static function check(): bool
    {
        return (static::$user instanceof User);
    }

    public static function getUser(): ?User
    {
        return static::$user;
    }

    public static function init(): void
    {
        $user = null;
        $user_cookie = u_get('user', $_COOKIE);
        $user_id = u_get('user_id', $_SESSION);

        if ($user_id) {
            $user = User::find($user_id);
        } elseif ($user_cookie) {
            $user_cookie = base64_decode($user_cookie);

            if ($user_cookie) {
                list($id, $remember_token) = array_pad(explode(';', $user_cookie, 2), 2, null);

                $user = User::getFirstWhere([
                    'id' => $id,
                    'remember_token' => $remember_token,
                ]);
            }
        }

        if ($user) {
            static::$user = $user;
        }
    }

    public static function login(int $user_id, ?string $remember_token = null): void
    {
        $_SESSION['user_id'] = $user_id;

        if ($remember_token) {
            $hash = base64_encode($user_id . ';' . $remember_token);
            $expires = time() + (60 * 60 * 24 * 365);
            setcookie('user', $hash, $expires, '/');
        }
    }

    public static function logout(): void
    {
        if (static::$user) {
            unset($_SESSION['user_id']);
            setcookie('user', null, time() - 1, '/');
            static::$user = null;        
        }
    }

    public static function user(): ?User
    {
        return static::$user;
    }
}
