<?php

namespace Core;

use PDO;
use PDOStatement;

class DB
{
    private static $instance = null;

    private function __clone() {}

    private function __construct() {}

    public static function getInstance(): PDO
    {
        if (static::$instance === null) {
            $options = [
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ];

            try {
                static::$instance = new PDO('mysql:dbname=keiforum;host=localhost;port=8889', 'root', 'root', $options);
            } catch (PDOException $e) {
                throw new PDOException($e->getMessage(), intval($e->getCode()));
            }
        }

        return static::$instance;
    }

    public static function __callStatic($method, $args): mixed
    {
        $db = self::getInstance();
        return call_user_func_array(array($db, $method), $args);
    }
}
