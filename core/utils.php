<?php

use Core\Auth;
use Core\Formatter;
use Core\Request;
use Core\Router;
use Core\View;

function e(?string $string): string
{
    return htmlentities($string, ENT_HTML5);
}

function u_auth(): bool
{
    return Auth::check();
}

function u_array_indexed(array $array, string $prop = 'id'): array
{
    $indexed = [];

    foreach ($array as $item) {
        if (is_object($item)) {
            $indexed[$prop . '_' . $item->$prop] = $item;    
        }

        if (is_array($item)) {
            $indexed[$prop . '_' . $item[$prop]] = $item;
        }
    }

    return $indexed;
}

function u_camelcase(string $string): string
{
    return preg_replace_callback('/[ _-](.?)/', function (array $matches) {
        return strtoupper($matches[1]);
    }, $string);
}

function u_csrf_token()
{
    return Request::getCsrfToken();
}

function u_current_user()
{
    return Auth::getUser();
}

function u_datetime_local(?string $datetime = null): string
{
    if ($datetime) {
        $datetime = new DateTime($datetime . ' GMT');
        $timezone = new DateTimezone(getenv('APP_TIMEZONE'));
        $datetime->setTimezone($timezone);
    } else {
        $datetime = new DateTime('now', new DateTimezone(getenv('APP_TIMEZONE')));
    }

    return $datetime->format('Y-m-d H:i:s');
}

function u_embed(string $path, array $data = []): string 
{
    if (file_exists(APP_ROOT . '/actions/' . $path . '.php')) {
        extract($data);
        return require APP_ROOT . '/actions/' . $path . '.php';
    }

    if (file_exists(APP_ROOT . '/views/' . $path . '.php')) {
        return View::render($path, $data, null);
    }

    return null;
}

function u_error(string $name, ?string $default = null): ?string
{
    return Request::getError($name, $default);
}

function u_get(string $key, array $hash, $default = null, bool $trim = true)
{
    if (!is_array($hash) && !is_object($hash)) {
        return $default;
    }

    $hash = (array) $hash;
    $keys = explode('.', $key);
    $key = array_shift($keys);
    $return = $default;

    if (isset($hash[$key])) {
        $return = $hash[$key];

        if (count($keys)) {
            return u_get(implode('.', $keys), $hash[$key], $default, $trim);
        }
    }

    if ($trim && is_string($return)) {
        $return = trim($return);
    }

    return $return;
}

function u_gravatar(string $email, int $size = 128): string
{
    $default =  urlencode('https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/natalie-portman-pregnancy-rumour-1612793600.jpg?crop=0.668xw:1.00xh;0.145xw,0&resize=640:*');
    $hash = md5(strtolower(trim($email)));
    return 'https://www.gravatar.com/avatar/' . $hash . '?d=' . $default . '&s=' . $size;
}

function u_inflect($num, $one, $many): string
{
    return ((int) $num === 1) ? $one : $many;
}

function u_flash(string $name, $default = null)
{
    return Request::getFlash($name, $default);
}

function u_format(string $string): string
{
    return Formatter::format($string);
}

function u_old(string $name, $default = null)
{
    return Request::getInputOld($name, $default);
}

function u_print_json($data): void
{
    echo '<pre>' . json_encode($data, JSON_PRETTY_PRINT) . '</pre>';
}

function u_random_string(int $length, bool $use_openssl = false): string
{
    if ($use_openssl) {
        $length = ceil($length * 0.66666);
        $bytes = openssl_random_pseudo_bytes($length);
        $string = base64_encode($bytes);
    } else {
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $max = strlen($chars) - 1;
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $chars[random_int(0, $max)];
        }
    }

    return $string;
}

function u_slugify(string $string): string
{
    $string = htmlentities(strtolower($string));
    $string = preg_replace('/&([a-z])(acute|cedil|circ|ring|tilde|uml);/', '$1', $string);
    $string = preg_replace('/([^a-z0-9]+)/', '-', html_entity_decode($string));
    $string = trim($string, '-');
    return $string;
}

function u_starts_with(string $pattern, string $subject): bool
{
    $len = strlen($pattern);
    return (substr($subject, 0, $len) === $pattern);
}

function u_time_ago(string $gmt_datetime): string
{
    $now = new DateTime('now', new DateTimezone(getenv('APP_TIMEZONE')));
    $yesterday = $now->sub(new DateInterval('P1D'));
    $datetime = new DateTime($gmt_datetime . ' GMT');
    $timezone = new DateTimezone(getenv('APP_TIMEZONE'));
    $datetime->setTimezone($timezone);
    $diff = $now->diff($datetime);
    $time = $datetime->format('G:i');

    if ($datetime->format('Ymd') === $yesterday->format('Ymd')) {
        return 'gisteren om ' . $time;
    }

    if ($diff->days === 0) {
        if ($diff->h === 0) {
            if ($diff->i > 0) {
                return $diff->i . ' min geleden';
            } else {
                return $diff->s . ' sec geleden';
            }
        }

        return 'vandaag om ' . $time;
    }

    if ($datetime->format('Ymd') > $now->format('Ymd') - 6) {
        $format = '%A';
    } else {
        $format = ($datetime->format('Y') === $now->format('Y')) ? '%e %b' : '%e %b %Y';
    }

    return strftime($format, $datetime->getTimestamp()) . ' om ' . $time;
}

function u_view(string $view, array $data = [], ?string $layout = 'app'): string
{
    return View::render($view, $data, $layout);
}