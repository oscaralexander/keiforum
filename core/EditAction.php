<?php

namespace App\Actions\Profile;

use App\Actions\Action;
use App\Models\Area;
use App\Models\User;
use Core\Request;
use Core\Router;
use Core\View;

class EditAction extends Action
{
    public function run(Router $router): string
    {
        Data::set('errors', Request::getErrors());
        Data::set('path', [
            ['href' => '/', 'label' => 'Home'],
        ]);

        return View::render('profile/edit');
    }
}
