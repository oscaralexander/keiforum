<?php

namespace Core;

class Cache
{
    private function __construct() {}

    public static function get(string $path): mixed 
    {
        $path = str_replace('-', '_', $path);
        $files = glob(APP_ROOT . '/cache/' . $path . '-*');

        if (count($files)) {
            $last_file = array_pop($files);
            preg_match('/-(\d+)$/', $last_file, $matches);

            if (count($matches)) {
                $timestamp = intval($matches[1]);

                if ($timestamp > time()) {
                    $string = file_get_contents($last_file);
                    $data = @unserialize($string);

                    if ($string === 'b:0;' || $data !== false) {
                        return $data;
                    }

                    return $string;
                }
            }
        }

        return null;
    }

    public static function purge(string $path): void
    {
        array_map('unlink', glob(APP_ROOT . '/cache/' . $path));
    }

    public static function set(string $path, mixed $data, int $expires): void
    {
        $dir = APP_ROOT . '/cache';
        $path = str_replace('-', '_', $path);
        $path_parts = explode('/', $path);
        array_pop($path_parts);

        foreach ($path_parts as $path_part) {
            $dir .= '/' . $path_part;

            if (!is_dir($dir)) {
                mkdir($dir);
            }
        }

        array_map('unlink', glob(APP_ROOT . '/cache/' . $path . '-*'));
        $path = APP_ROOT . '/cache/' . $path . '-' . (time() + $expires);
        $data = (is_array($data) || is_object($data)) ? serialize($data) : $data;
        file_put_contents($path, $data);
    }
}
