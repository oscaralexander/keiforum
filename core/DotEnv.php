<?php

namespace Core;

class DotEnv
{
    const PROCESS_BOOLEANS = 'PROCESS_BOOLEANS';

    protected $options = [];
    protected $path;

    public function __construct(string $path, array $options = [])
    {
        if (!file_exists($path)) {
            throw new \InvalidArgumentException(sprintf('File does not exist: %s', $path));
        }

        $this->path = $path;
        $this->processOptions($options);
    }

    public function load(): void
    {
        if (!is_readable($this->path)) {
            throw new \RuntimeException(sprintf('File is not readable: %s', $this->path));
        }

        $lines = file($this->path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        foreach ($lines as $line) {
            if (strpos(trim($line), '#') === 0) {
                continue;
            }

            list($name, $value) = explode('=', $line, 2);
            $name = trim($name);
            $value = $this->processValue($value);

            if (!array_key_exists($name, $_ENV) && !array_key_exists($name, $_SERVER)) {
                putenv(sprintf('%s=%s', $name, $value));
                $_ENV[$name] = $value;
                $_SERVER[$name] = $value;
            }
        }
    }

    private function processOptions(array $options): void
    {
        $this->options = array_merge([
            static::PROCESS_BOOLEANS => true
        ], $options);
    }

    private function processValue(string $value): mixed
    {
        $value = trim($value);

        if (!empty($this->options[static::PROCESS_BOOLEANS])) {
            $value_lc = strtolower($value);
            $is_boolean = in_array($value_lc, ['true', 'false'], true);

            if ($is_boolean) {
                return ($value_lc === 'true');
            }
        }

        return $value;
    }
}