<?php

namespace Core;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Mail
{
    protected $mailer;

    public function __construct()
    {
        $this->mailer = new PHPMailer(true);
        $this->mailer->CharSet = 'UTF-8';
        $this->mailer->Host = getenv('SMTP_HOST');
        $this->mailer->Password = getenv('SMTP_PASSWORD');
        $this->mailer->Port = 587;
        $this->mailer->SMTPAuth = true;
        $this->mailer->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $this->mailer->Username = getenv('SMTP_USERNAME');
        $this->mailer->isHTML(true);
        $this->mailer->isSMTP();
        $this->mailer->setFrom(getenv('APP_EMAIL_FROM'), getenv('APP_EMAIL_FROM_NAME'));
    }

    public function addRecipient(string $email, string $name = ''): void
    {
        $this->mailer->addAddress($email, $name);
    }

    public function send(): bool
    {
        $success = $this->mailer->send();

        if (!$success && getenv('APP_DEBUG')) {
            throw new Exception($this->mailer->ErrorInfo);
        }

        return $success;
    }

    public function setBody(string $body): void
    {
        $this->mailer->Body = $body;
    }

    public function setSubject(string $subject): void
    {
        $this->mailer->Subject = $subject;
    }
}
