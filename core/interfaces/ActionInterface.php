<?php

namespace Core\Interfaces;

use Core\Router;

interface ActionInterface
{
    public function __construct(Router $router);
    public function isProtected(): bool;
    public function run(): string;
}