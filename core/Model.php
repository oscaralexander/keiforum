<?php

namespace Core;

use PDO;
use ReflectionClass;
use ReflectionProperty;

class Model
{
    protected static $primary_key = 'id';
    protected static $table;
    protected $props = [];

    public function __construct(array $data = [])
    {
        $data = static::sanitize($data);

        foreach ($data as $col => $value) {
            $this->$col = $value;
        }
    }

    public function __get(string $name): mixed
    {
        $method = 'get' . ucfirst(u_camelcase($name)) . 'Attribute';

        if (method_exists($this, $method)) {
            return $this->$method();
        }

        if (isset($this->props[$name])) {
            return $this->props[$name];
        }

        return null;
    }

    public function __isset(string $name): bool
    {
        $method = 'get' . ucfirst(u_camelcase($name)) . 'Attribute';

        if (method_exists($this, $method)) {
            return true;
        }

        return isset($this->props[$name]);
    }

    public function __set(string $name, mixed $value): void
    {
        $this->props[$name] = $value;
    }

    /**
     * Class methods
     */

    public static function all(): array
    {
        $sth = DB::query('SELECT * FROM ' . static::$table);
        return $sth->fetchAll(PDO::FETCH_CLASS, get_called_class());
    }

    public static function count(array $where, string $count_col = 'id'): int
    {
        $where = static::sanitize($where, true);
        $cols = array_keys($where);
        $sql_where = [];

        foreach ($where as $col => $value) {
            if ($value === null) {
                $sql_where[] = $col . ' IS NULL';
                unset($where[$col]);
            } else {
                $sql_where[] = $col . ' = :' . $col;
            }
        }

        $sql_where = implode(' AND ', $sql_where);
        $sql = 'SELECT COUNT(' . $count_col . ') AS count FROM ' . static::$table . ' WHERE ' . $sql_where;
        $sth = DB::prepare($sql);
        $sth->execute($where);
        return intval($sth->fetchColumn());
    }

    public static function create(array $data): ?self
    {
        $data = static::sanitize($data);

        if (property_exists(get_called_class(), 'created_at')) {
            $data['created_at'] = gmdate('Y-m-d H:i:s');
        }

        $cols = array_keys($data);
        $sql_cols = implode(', ', $cols);
        $sql_markers = ':' . implode(', :', $cols); 
        $sql = 'INSERT INTO ' . static::$table . ' (' . $sql_cols . ') VALUES (' . $sql_markers . ')';
        $sth = DB::prepare($sql);

        if ($sth->execute($data)) {
            $data['id'] = DB::lastInsertId();
            return new static($data);
        }

        return null;
    }

    public static function delete(mixed $where): bool
    {
        $bindings = [];
        $sql_where = static::getSqlWhere($where, $bindings);
        $sql = 'DELETE FROM ' . static::$table . ' WHERE ' . $sql_where;
        $sth = DB::prepare($sql);
        return $sth->execute($bindings);
    }

    public static function exists(array $where, string $col = 'id'): bool
    {
        return (static::count($where, $col) > 0);
    }

    public static function find(int $id): ?self
    {
        $sql = 'SELECT * FROM ' . static::$table . ' WHERE id = :id';
        $sth = DB::prepare($sql);
        $sth->bindParam('id', $id, PDO::PARAM_INT);
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_CLASS, get_called_class());
        $result = $sth->fetch();
        return $result ?? null;
    }

    public static function findAll(array $ids): array
    {
        $ids = array_filter($ids);
        $ids = array_unique($ids);
        $ids = array_values($ids);

        if (count($ids)) {
            $in = implode(', ', array_fill(0, count($ids), '?'));
            $sql = 'SELECT * FROM ' . static::$table . ' WHERE id IN (' . $in . ')';
            $sth = DB::prepare($sql);
            $sth->execute($ids);
            return $sth->fetchAll(PDO::FETCH_CLASS, get_called_class());
        }

        return [];
    }

    public static function findBySlug(string $slug): ?self
    {
        return static::getFirstWhere(['slug' => $slug]);
    }

    public static function getFirstWhere(array $where, ?string $order_by = null): ?self
    {
        $rows = static::getWhere($where, $order_by, 1, 0);
        return count($rows) ? $rows[0] : null;
    }

    public static function getWhere(array $where, ?string $order_by = null, ?int $limit = null, ?int $offset = null): array
    {
        $rows = [];

        if (count($where)) {
            $bindings = [];
            $sql_where = static::getSqlWhere($where, $bindings);
            $sql = 'SELECT * FROM ' . static::$table . ' WHERE ' . $sql_where;

            if ($order_by) {
                $sql .= ' ORDER BY ' . $order_by;
            }

            if ($limit) {
                $sql .= ' LIMIT ' . $limit;

                if ($offset) {
                    $sql .= ', ' . $offset;
                }
            }

            $sth = DB::prepare($sql);
            $sth->execute($bindings);
            $rows = $sth->fetchAll(PDO::FETCH_CLASS, get_called_class());
        }

        return $rows;
    }

    public static function update(array $data, array|int $where): bool
    {
        $bindings = [];
        $data = static::sanitize($data, true);
        $sql_data = [];

        if (property_exists(get_called_class(), 'updated_at')) {
            $data['updated_at'] = gmdate('Y-m-d H:i:s');
        }

        foreach ($data as $col => $value) {
            $bindings[$col] = $value;
            $sql_data[] = "{$col} = :{$col}";
        }

        $sql_data = implode(', ', $sql_data);
        $sql_where = static::getSqlWhere($where, $bindings);

        $sql = 'UPDATE ' . static::$table . ' SET ' . $sql_data . ' WHERE ' . $sql_where;
        $sth = DB::prepare($sql);

        return $sth->execute($bindings);
    }

    /**
     * Private class methods
     */

    protected static function getSqlWhere(array|int $where, array &$bindings): ?string
    {
        if (is_int($where)) {
            $where = [static::$primary_key => $where];
        }

        $sql = null;
        $where = static::sanitize($where);

        foreach ($where as $col => $value) {
            if ($value === null) {
                $fields[] = $col . ' IS NULL';
            } else {
                if (is_array($value)) {
                    $in = [];
                    $values = array_values($value);

                    for ($i = 0; $i < count($values); $i++) {
                        $in['w_in' . $i] = $values[$i];
                    }

                    $bindings = array_merge($bindings, $in);
                    $fields[] = $col . ' IN (:' . implode(', :', array_keys($in)) . ')';
                } else {
                    if ($value === 'NULL') {
                        $fields[] = $col . ' IS NULL';
                    } elseif ($value === 'IS NOT NULL') {
                        $fields[] = $col . ' IS NOT NULL';
                    } else {
                        $bindings['w_' . $col] = $value;
                        $fields[] = $col . ' = :w_' . $col;
                    }
                }
            }

            $sql = implode(' AND ', $fields);
        }

        return $sql;
    }

    protected static function sanitize(array $data): array
    {
        $reflect = new ReflectionClass(get_called_class());
        $cols = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);
        $cols = array_flip(array_column($cols, 'name'));

        $data = array_map(function ($value) {
            if (is_bool($value)) {
                return ($value === true) ? 1 : 0;
            }

            return $value;
        }, $data);

        return array_intersect_key($data, $cols);
    }
}
