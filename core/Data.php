<?php

namespace Core;

class Data
{
    protected static $data = [];

    private function __construct() {}

    public static function all(): array
    {
        return static::$data;
    }

    public static function get(string $name, mixed $default = null): mixed
    {
        return static::$data[$name] ?? $default;
    }

    public static function set(mixed $name, mixed $value = null):void
    {
        if (is_array($name)) {
            static::$data = array_merge(static::$data, $name);
        } else {
            static::$data[$name] = $value;
        }
    }

    public static function unset(string $name):void
    {
        unset(static::$data[$name]);
    }
}
