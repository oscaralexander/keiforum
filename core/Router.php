<?php

namespace Core;

class Router
{
    private static $action = null;
    private static $instance = null;
    private static $params = [];

    private function __clone() {}
    private function __construct() {}

    /**
     * Class methods
     */

    public static function getInstance(): self
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private static function resolve(string $request, array $routes, string $namespace = ''): ?string
    {
        foreach ($routes as $key => $val) {
            if (is_array($val)) {
                $action = static::resolve($request, $val, $namespace . '\\' . $key);

                if ($action) {
                    return $action;
                }
            } else {
                if (preg_match('#^' . $key . '$#', $request, static::$params)) {
                    return ltrim($namespace . '\\' . $val, '\\');
                }
            }
        }

        return null;
    }

    public function route(array $routes): ?string
    {       
        $path = parse_url(Request::getURI(), PHP_URL_PATH);
        $path = trim($path, '/');
        $request = Request::getMethod() . ':' . $path;
        static::$action = static::resolve($request, $routes);
        return static::$action;
    }

    /**
     * Instance methods
     */

    public function getAction(): string
    {
        return static::$action;
    }

    public function getParam(string $key, $default = null): mixed
    {
        return u_get($key, static::$params, $default);
    }

    public function getParams(): array
    {
        return static::$params;
    }
}
