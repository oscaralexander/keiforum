<?php

namespace Core;

use Core\DB;

class Validator
{
    private $errors = [];
    private $input = [];
    private $rules = [];

    public function __construct(array $rules)
    {
        $this->rules = $rules;
    }

    public function addError(string $field, string $error): void
    {
        if (!isset($this->errors[$field])) {
            $this->errors[$field] = [];
        }

        $this->errors[$field][] = $error;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function validate(array $input): bool
    {
        $this->input = $input;

        foreach ($this->rules as $field => $rules) {
            foreach ($rules as $rule => $details) {
                $is_optional = (strpos($rule, '?') === 0);
                $rule = ltrim($rule, '?');

                if (is_array($details)) {
                    $arg = $details['arg'] ?? null;
                    $error = $details['error'] ?? '';
                } else {
                    $arg = null;
                    $error = $details;
                }

                $method = 'validate' . ucfirst(u_camelcase($rule));

                if (method_exists($this, $method)) {
                    $value = u_get($field, $this->input, '');

                    if ($is_optional && empty($value)) {
                        continue;
                    }

                    $is_valid = $this->$method($field, $value, $arg);

                    if (!$is_valid) {
                        if (!isset($this->errors[$field])) {
                            $this->errors[$field] = [];
                        }

                        $this->errors[$field][] = $error;
                    }
                }
            }
        }

        return (count($this->errors) === 0);
    }

    private function validateAccepted(string $field, string $value): bool
    {
        return ($value == 1 || $value == 'on');
    }

    private function validateConfirmed(string $field, string $value): bool
    {
        $value_confirm = u_get($field . '_confirm', $this->input);
        return ($value === $value_confirm);
    }

    private function validateDate(string $field, string $value): bool
    {
        $date = explode('-', $value);

        if (count($date) !== 3) {
            return false;
        }

        return checkdate($date[1], $date[2], $date[0]);
    }

    private function validateEmail(string $field, string $value): bool
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    private function validateExists(string $field, string $value, string $in): bool
    {
        $in = explode('.', $in);
        $table = $in[0];
        $col = $in[1] ?? $field;
        $sth = DB::prepare("SELECT COUNT(`$col`) FROM `$table` WHERE `$col` = :value");
        $sth->execute(['value' => $value]);

        return ($sth->fetchColumn() > 0);
    }

    private function validateIn(string $field, string $value, array $in): bool
    {
        return in_array($value, $in);
    }

    private function validatePassword(string $field, string $value): bool
    {
        $required_digits = getenv('APP_PASSWORD_REQUIRED_DIGITS');
        $required_length = getenv('APP_PASSWORD_REQUIRED_LENGTH');
        
        preg_match_all('/[0-9]/', $value, $matches_digits);
        $digits = $matches_digits[0] ?? [];

        preg_match_all('/[a-z]/', $value, $matches_lowercase);
        $lowercase = $matches_lowercase[0] ?? [];

        preg_match_all('/[A-Z]/', $value, $matches_uppercase);
        $uppercase = $matches_uppercase[0] ?? [];

        $is_valid_digits = (count($digits) >= $required_digits);
        $is_valid_length = (strlen($value) >= $required_length);
        $is_valid_mixed_case = (count($lowercase) > 0 && count($uppercase) > 0);
        
        return ($is_valid_digits && $is_valid_length && $is_valid_mixed_case);
    }

    private function validateRequired(string $field, string $value): bool
    {
        return (strlen(trim($value)) > 0);
    }

    private function validateUnique(string $field, string $value, string $in): bool
    {
        $in = explode('.', $in);
        $table = $in[0];
        $col = $in[1] ?? $field;
        $sth = DB::prepare("SELECT COUNT(`$col`) FROM `$table` WHERE `$col` = :value");
        $sth->execute(['value' => $value]);

        return ($sth->fetchColumn() === 0);
    }
}
