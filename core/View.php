<?php

namespace Core;

use Core\Data;

class View
{
    public static function render(string $view, array $data = [], ?string $layout = 'app'): string
    {
        extract(Data::all());
        extract($data);
        ob_start();
        require APP_ROOT . '/views/' . $view . '.php';

        if ($layout) {
            $content_for_layout = ob_get_clean();
            ob_start();
            require APP_ROOT . '/views/_layouts/' . $layout . '.php';
        }

        return ob_get_clean();
    }
}
