<?php

namespace Core;

class Formatter
{
    private function __construct() {}

    public static function format(string $text): string
    {
        $lines = explode(PHP_EOL, $text);
        $text = static::formatBlocks($lines);
        return $text;
    }

    protected static function getCleanLine(string $line): string
    {
        return preg_replace('/^ *(\#|\*)? /', '', $line);
    }
 
    protected static function getFirstChar(string $line): string
    {
        return substr(trim($line), 0, 1);
    }

    protected static function getIndent(string $line): int
    {
        preg_match('/^ *(\#|\*)? /', $line, $matches);
        return count($matches) ? strlen($matches[0]) : 0;
    }

    public static function formatBlocks(array &$lines, int $current_indent = 0): string
    {
        $block_open = false;
        $block_el = null;
        $li_open = false;
        $text = '';

        while (count($lines)) {
            $line = $lines[0];
            $first_char = static::getFirstChar($line);
            $indent = (trim($line) === '') ? $current_indent : static::getIndent($line);

            if ($indent === $current_indent) {
                if (!$block_open) {
                    if ($first_char === '#') {
                        $block_el = 'ol';
                    } elseif ($first_char === '*') {
                        $block_el = 'ul';
                    } else {
                        $block_el = 'p';
                    }

                    $text .= '<' . $block_el . '>';
                    $block_open = true;
                }

                if ($first_char === '#' || $first_char === '*') {
                    if ($li_open) {
                        $text .= '</p></li>';
                    }

                    $text .= '<li><p>';
                    $li_open = true;
                }

                $line = static::getCleanLine(array_shift($lines));
                $text .= $line . '<br>';
            } elseif ($indent === $current_indent + 2) {
                if ($block_open) {
                    $text .= '</p>';

                    if ($block_el === 'p') {
                        $block_open = false;
                    }
                }

                $text .= static::formatBlocks($lines, $current_indent + 2);
            } elseif ($indent === $current_indent - 2) {
                if ($block_open) {
                    if ($li_open) {
                        $text .= '</p></li>';
                        $li_open = false;
                    }

                    $text .= '</' . $block_el . '>';
                    $block_open = false;
                }

                return static::sanitize($text);
            }
        }

        if ($block_open) {
            $text .= '</' . $block_el . '>';
            $block_open = false;
        }

        return static::sanitize($text);
    }

    public static function sanitize(string $text): string
    {
        $text = str_replace('</ol></p>', '</ol>', $text);
        $text = str_replace('</ul></p>', '</ul>', $text);
        // $text = str_replace('<br></p>', '</p>', $text);
        return $text;
    }
}