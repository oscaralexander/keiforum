<?php

namespace Core;

use Core\Auth;
use Core\Request;
use Core\View;

class Response
{
    public static function error(int $http_code, array $data = []): string
    {
        http_response_code($http_code);
        $response = View::render('_errors/' . $http_code, $data);
        $response = static::gzip($response);
        die($response);
    }

    public static function gzip(string $response): string
    {
        if (strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false) {
            $response = gzencode(trim(preg_replace('/\s+/', ' ', $response)), 9);
            header('Content-Encoding: gzip');
            header('Content-Length: ' . strlen($response));
            header('Vary: Accept-Encoding');
        }

        return $response;   
    }

    public static function json(array $data, int $http_code = 200): void
    {
        http_response_code($http_code);
        header('Content-type: application/json; charset=utf-8');
        $response = json_encode($data);
        $response = static::gzip($response);
        die($response);
    }

    public static function jsonOrRedirect(string $to, array $data = []): void
    {
        if (Request::isAjax()) {
            static::json($data);
        } else {
            static::redirect($to, $data);
        }
    }

    public static function redirect(string $to, array $data = []): void
    {
        if (!empty($data)) {
            Request::setFlash($data);
        }

        if (Request::isAjax()) {
            header('Content-type: application/json; charset=utf-8');
            die(json_encode(['location' => $to]));
        }

        header('Location: ' . $to, true, 302);      
        exit();
    }
}
