<?php

$time_start = (float) microtime(true);

define('ROOT', realpath(__dir__ . '/..'));
define('APP_ROOT', ROOT . '/app');

require ROOT . '/vendor/autoload.php';

use Core\Auth;
use Core\DotEnv;
use Core\Request;
use Core\Response;
use Core\Router;

session_start();
$env = new DotEnv(ROOT . '/.env');
$env->load();

setlocale(LC_ALL, getenv('APP_LOCALE'));
date_default_timezone_set(getenv('APP_TIMEZONE'));

if (getenv('APP_DEBUG')) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}

Auth::init();
Request::init();

if (Request::isPost() && !Request::validateCsrfToken()) {
    $response = Response::error(403, ['message' => 'CSRF token mismatch.']);
} else {
    $routes = require APP_ROOT . '/routes.php';
    $router = Router::getInstance();
    $action_class = $router->route($routes);
    
    if ($action_class) {
        header('Content-Type: text/html');
        $action_class = '\\App\\Actions\\' . $action_class;
    
        try {
            $action = new $action_class($router);

            if ($action->isProtected() && !Auth::check()) {
                $response = Response::error(403, ['message' => 'Voor deze pagina moet je ingelogd zijn.']);
            } else {
                $response = $response = $action->run();
            }
        } catch (Exception $e) {
            $response = Response::error(500, ['exception' => $e]);
        }
    } else {
        $response = Response::error(404);
    }
}

$time_elapsed = (float) microtime(true) - $time_start;
$memory = number_format(memory_get_peak_usage() / 1024000, 1) . 'MB';

$response .= '<!-- Page generated in ' . number_format($time_elapsed, 3) . ' seconds, using ' . $memory . ' memory. -->';

echo Response::gzip($response);
exit();
