export default class Textarea
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            ...options,
        }

        this.initListeners()
    }

    initListeners() {
        this.$el.addEventListener('input', this.onInput.bind(this), false)
    }

    onInput() {
        this.$el.style.height = 'auto'
        this.$el.style.height = this.$el.scrollHeight + 'px'
    }
}
