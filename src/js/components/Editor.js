export default class Editor
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            ...options,
        }

        this.init()
        this.initListeners()
    }

    init() {
        const id = this.$el.dataset.editorId

        this.editor = RedactorX('#editor-' + id, {
            buttons: {
                addbar: ['paragraph', 'image', 'embed', 'quote', 'pre'],
            },
            editor: {
                drop: false,
                lang: 'nl',
            },
            embed: {
                checkbox: true,
            },
            format: ['p', 'ol', 'ul'],
            image: {
                upload: function(upload, data) {
                },
                name: 'file',
                newtab: true,
            },
            link: {
                target: '_blank',
            },
            quote: {
                template: '<blockquote><p>Citaat</p></blockquote>',
            },
            source: false,
            styles: false,
            topbar: false,
        })

        this.id = id
    }

    initListeners() {
    }
}

/**
    <script>
        $R('#content', {
            buttons: ['format', 'bold', 'italic', 'deleted', 'link', 'image'],
            buttonsTextLabeled: false,
            formatting: [
                'p',
                'blockquote',
                'pre',
            ],
            imageUpload: function(formData, files, event) {
                return 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/natalie-portman-defund-the-police-wit-privilege-1591783787.jpg';
            },
            imageEditable: false,
            imageFigure: false,
            lang: 'nl',
            linkNewTab: false,
            linkTarget: '_blank',
            maxHeight: '800px',
            minHeight: '112px',
            styles: false,
            toolbarExternal: '#editorToolbar',
        });

        $R('#content', 'module.block.format', {
            tag: 'blockquote',
            type: 'toggle',
        });
    </script>
 */
