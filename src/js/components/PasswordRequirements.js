export default class PasswordRequirements
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            classNameIsChecked: 'is-checked',
            requiredDigits: 2,
            requiredLength: 12,
            selector: '.js-passwordRequirements',
            selectorDigits: '.js-passwordRequirementsDigits',
            selectorLength: '.js-passwordRequirementsLength',
            selectorMixedCase: '.js-passwordRequirementsMixedCase',
            ...options
        }

        this.init()
        this.initListeners()
        this.validate()
    }

    init() {
        const input = this.$el.dataset.passwordRequirementsInput
        this.$input = document.getElementById(input)

        this.$digits = this.$el.querySelector(this.options.selectorDigits)
        this.$mixedCase = this.$el.querySelector(this.options.selectorMixedCase)
        this.$length = this.$el.querySelector(this.options.selectorLength)
    }

    initListeners() {
        if (this.$input) {
            this.$input.addEventListener('input', this.onInput.bind(this), false)
        }
    }

    onInput() {
        this.validate()
    }

    validate() {
        const requiredDigits = parseInt(this.$el.dataset.passwordRequirementsDigits || this.options.requiredDigits, 10)
        const requiredLength = parseInt(this.$el.dataset.passwordRequirementsLength || this.options.requiredLength, 10)
        const val = this.$input.value

        const checkDigits = ((val.match(/[0-9]/g) || []).length >= requiredDigits)
        this.$digits.classList.toggle(this.options.classNameIsChecked, checkDigits)

        const lowercase = val.match(/[a-z]/g) || []
        const uppercase = val.match(/[A-Z]/g) || []
        const checkMixedCase = (lowercase.length > 0 && uppercase.length > 0)
        this.$mixedCase.classList.toggle(this.options.classNameIsChecked, checkMixedCase)

        const checkLength = (val.length >= requiredLength)
        this.$length.classList.toggle(this.options.classNameIsChecked, checkLength)
    }
}
