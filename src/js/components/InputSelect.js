export default class InputSelect
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            classNameEmpty: 'is-empty',
            classNameFocus: 'is-focus',
            classNameLabel: 'inputSelect__label',
            classNameSelect: 'inputSelect__select',
            classNameWrapper: 'inputSelect',
            ...options
        }

        this.init()
        this.initListeners()
    }

    init() {
        this.$el.classList.add(this.options.classNameSelect)

        this.$wrapper = document.createElement('div')
        this.$wrapper.classList.add(this.options.classNameWrapper)
        this.$el.parentNode.insertBefore(this.$wrapper, this.$el)
        this.$wrapper.appendChild(this.$el)

        this.$label = document.createElement('div')
        this.$label.classList.add(this.options.classNameLabel)
        this.$label.innerText = this.label
        this.$el.parentNode.insertBefore(this.$label, this.$el.nextSibling)
    }

    initListeners() {
        this.$el.addEventListener('change', this.onChange.bind(this))
        window.requestAnimationFrame(this.onChange.bind(this))
    }

    onChange() {
        this.$wrapper.classList.toggle(this.options.classNameEmpty, (this.value === ''))
        this.$label.innerText = this.label
    }

    get label() {
        if (this.$el.selectedIndex > -1) {
            return this.$el.options[this.$el.selectedIndex].innerText.trim()
        }

        return this.$el.options[0].innerText.trim()
    }

    get value() {
        return this.$el.value
    }
}
