export default class Editor
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            defaultBoldText: 'dikgedrukt',
            defaultItalicText: 'cursief',
            defaultStrikeThroughText: 'doorstreept',
            defaultLinkText: 'tekst',
            defaultLinkUrl: 'https://',
            defaultQuoteText: 'Citaat',
            selectorAction: '.js-editorAction',
            selectorInput: '.js-editorInput',
            selectorModalImage: '.js-modalImage',
            selectorModalLink: '.js-modalLink',
            selectorModalLinkInputText: 'input[name="link_text"]',
            selectorModalLinkInputUrl: 'input[name="link_url"]',
            selectorShadow: '.js-editorShadow',
            ...options,
        }

        this.init()
        this.initListeners()
    }

    init() {
        this.$$action = this.$el.querySelectorAll(this.options.selectorAction)
        this.$input = this.$el.querySelector(this.options.selectorInput)
        this.$modalImage = this.$el.querySelector(this.options.selectorModalImage)
        this.$modalLink = this.$el.querySelector(this.options.selectorModalLink)
        this.$modalLinkInputText = this.$el.querySelector(this.options.selectorModalLinkInputText)
        this.$modalLinkInputUrl = this.$el.querySelector(this.options.selectorModalLinkInputUrl)
        this.hasFocus = false
        this.selection = null

        document.execCommand('defaultParagraphSeparator', false, 'p')
    }

    initListeners() {
        this.$$action.forEach($action => $action.addEventListener('click', this.onActionClick.bind(this)))
        this.$input.addEventListener('focusout', this.onInputFocusOut.bind(this), false)

        if (this.$modalImage) {
            this.$modalImage.addEventListener('modalimage:submit', this.onModalImageSubmit.bind(this), false)
        }

        if (this.$modalLink) {
            this.$modalLink.addEventListener('modallink:submit', this.onModalLinkSubmit.bind(this), false)
        }
    }

    getSelection() {
        const value = this.$input.value

        let selectionStart = this.$input.selectionStart
        let selectionEnd = this.$input.selectionEnd
        let text = value.substring(selectionStart, selectionEnd)
        let textBefore = value.substring(0, selectionStart)
        let textAfter = value.substring(selectionEnd)

        if (selectionStart !== selectionEnd) {
            /*
            if (!allowNewlines) {
                const newlines = /\r|\n/.exec(text)

                if (newlines) {
                    selectionEnd = selectionEnd - (text.length - newlines.index)
                    text = text.substring(0, newlines.index)
                }
            }
            */

            const spaceBefore = /^\s+/.exec(text)

            if (spaceBefore) {
                selectionStart = selectionStart + spaceBefore[0].length
                text = text.substring(spaceBefore[0].length)
            }

            const spaceAfter = /\s+$/.exec(text)

            if (spaceAfter) {
                selectionEnd = selectionEnd - spaceAfter[0].length
                text = text.substring(0, spaceAfter.index)
            }

            textBefore = value.substring(0, selectionStart)
            textAfter = value.substring(selectionEnd)
        }

        return {
            selectionEnd,
            selectionStart,
            text,
            textAfter,
            textBefore
        }
    }

    insertBlock(symbol, placeholder = '') {
        let selection = this.getSelection(true)
        let text = selection.text.trim() || placeholder
        let textBefore = selection.textBefore.trim()
        let textAfter = selection.textAfter.trim()

        if (textBefore !== '') {
            textBefore = textBefore + '\n\n'
        }

        if (textAfter !== '') {
            textAfter = '\n\n' + textAfter
        }

        text = symbol + ' ' + text.replaceAll('\n', '\n' + symbol + ' ')
        this.$input.value = textBefore + text + textAfter
        this.$input.focus()
        this.$input.setSelectionRange(textBefore.length + 2, textBefore.length + text.length)
        this.updateShadow()
    }

    insertLink() {
        this.$modalLinkInputText.value = this.selection.text

        window.requestAnimationFrame(() => {
            this.$modalLinkInputUrl.focus()
        })
    }

    onActionClick(e) {
        e.preventDefault()
        const action = e.currentTarget.dataset.action

        switch (action) {
            case 'a':
                this.insertLink()
                break;

            case 'b':
                this.wrapSymbol('*', this.options.defaultBoldText)
                break;

            case 'i':
                this.wrapSymbol('_', this.options.defaultItalicText)
                break;    

            case 's':
                this.wrapSymbol('~', this.options.defaultStrikeThroughText)
                break;

            case 'quote':
                this.wrapBlock('quote', this.options.defaultQuoteText)
                break;
        }
    }

    onInputFocusOut() {
        this.selection = this.getSelection()
    }

    onModalImageSubmit(e) {
        this.insertBlock(e.detail.url)
    }

    onModalLinkSubmit(e) {
        if (this.selection) {
            const text = e.detail.text || this.options.defaultLinkText
            const url = e.detail.url || this.options.defaultLinkUrl
            const textBefore = this.selection.textBefore + '[url=' + url + ']'
            const textAfter = '[/url]' + this.selection.textAfter

            this.$input.value = textBefore + text + textAfter
            this.$input.focus()
            this.$input.setSelectionRange(textBefore.length, textBefore.length + text.length)
        }
    }

    wrap(before, after, defaultText, allowNewLines) {
        const selection = this.getSelection()
        let text = selection.text || defaultText

        if (allowNewLines) {
            text = before + text + after
        } else {
            const lines = text.split(/\n/)           
            text = lines.map(line => before + line.trim() + after).join("\n")
        }

        const textBefore = selection.textBefore
        const textAfter = selection.textAfter

        this.$input.value = textBefore + text + textAfter
        this.$input.focus()
        this.$input.setSelectionRange(
            textBefore.length + before.length,
            textBefore.length + text.length - after.length
        )
    }

    wrapSymbol(symbol, defaultText) {
        this.wrap(symbol, symbol, defaultText, false)
    }

    wrapTag(tag, defaultText) {
        this.wrap(`[${tag}]`, `[/${tag}]`, defaultText)
    }
}
