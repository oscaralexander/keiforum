import axios from 'axios'

export default class Upload
{
    constructor($el, file = null, options = {}) {
        this.$el = $el
        this.file = file
        this.options = {
            classNameIsUploaded: 'is-uploaded',
            classNameIsUploading: 'is-uploading',
            selectorCancel: '.js-uploadCancel',
            selectorInput: '.js-uploadInput',
            selectorPreview: '.js-uploadPreview',
            selectorProgressFill: '.js-uploadProgressFill',
            ...options,
        }

        this.init()
        this.initListeners()

        if (this.file) {
            this.setPreview()
            this.upload()
        }
    }

    init() {
        this.$cancel = this.$el.querySelector(this.options.selectorCancel)
        this.$input = this.$el.querySelector(this.options.selectorInput)
        this.$preview = this.$el.querySelector(this.options.selectorPreview)
        this.$progressFill = this.$el.querySelector(this.options.selectorProgressFill)
        this.cancelTokenSource = axios.CancelToken.source()
        this.isUploaded = false
        this.isUploading = false
    }

    initListeners() {
        this.$cancel.addEventListener('click', this.onCancelClick.bind(this), false)
    }

    onCancelClick() {
        if (this.isUploading) {
            this.cancelTokenSource.cancel()
            this.isUploading = false
        }

        this.$el.dispatchEvent(new CustomEvent('upload:cancel', { bubbles: true }))
        this.isUploading = false
    }

    onUploadComplete(response) {
        this.$el.dispatchEvent(new CustomEvent('upload:complete', {
            bubbles: true,
            detail: {
                deleteHash: response.data.data.deletehash,
                url: response.data.data.link,
            },
        }))

        this.$el.classList.add(this.options.classNameIsUploaded)
        this.$el.classList.remove(this.options.classNameIsUploading)
        this.$input.value = response.data.data.link
        this.isUploaded = true
        this.isUploading = false
    }

    onUploadError(exception) {
        if (axios.isCancel(exception)) {
        }

        this.isUploaded = false
        this.isUploading = false
    }

    onUploadProgress(e) {
        const percentage = Math.round((e.loaded * 100) / e.total)
        this.$progressFill.style.width = `${percentage}%`
    }

    setPreview() {
        const reader = new FileReader()

        reader.onload = e => {
            const $img = document.createElement('img')
            $img.src = e.target.result
            this.$preview.appendChild($img)
        }

        reader.readAsDataURL(this.file)
    }

    upload() {
        let data = new FormData()
        data.append('image', this.file)

        const request = {
            cancelToken: this.cancelTokenSource.token,
            data: data,
            headers: {
                'Authorization': 'Client-ID 4723c6e66f171e5',
                'Content-Type': 'application/x-www-form-urlencoded',
                'X-Rapidapi-Host': 'imgur-apiv3.p.rapidapi.com',
                'X-Rapidapi-Key': '51ee9b1714mshf9b85fcb84dfab7p1216e6jsne9526acae4fa',
            },
            method: 'POST',
            onUploadProgress: this.onUploadProgress.bind(this),
            url: 'https://imgur-apiv3.p.rapidapi.com/3/image',
        }

        axios(request)
            .then(this.onUploadComplete.bind(this))
            .catch(this.onUploadError.bind(this))

        this.$el.classList.add(this.options.classNameIsUploading)
    }
}
