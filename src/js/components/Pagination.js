export default class Pagination
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            selectorSelect: '.js-paginationSelect',
            ...options
        }

        this.init()
        this.initListeners()
    }

    init() {
        this.$$select = this.$el.querySelectorAll(this.options.selectorSelect)
        this.baseUrl = this.$el.getAttribute('action')
    }
    
    initListeners() {
        this.$$select.forEach($select => $select.addEventListener('change', this.onSelectChange.bind(this), false))
    }

    onSelectChange(e) {
        const $el = e.target
        const p = $el.value

        if (p !== '') {
            window.location = `${this.baseUrl}/${p}`
        }
    }
}
