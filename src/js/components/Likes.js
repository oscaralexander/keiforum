export default class Post
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            classNameIsChanged: 'is-changed',
            classNameIsLiked: 'is-liked',
            selector: '.js-likes',
            selectorCount: '.js-likesCount',
            ...options
        }

        this.init()
        this.initListeners()
    }

    init() {
        this.$count = this.$el.querySelector(this.options.selectorCount)
        this.$like = this.$el.querySelector(this.options.selectorLike)
        this.count = Number(this.$el.dataset.likesCount || 0)
        this.isLiked = this.$el.classList.contains(this.options.classNameIsLiked)
    }

    initListeners() {
        this.$el.addEventListener('xhrform:success', this.onLikeToggle.bind(this), false)
    }

    onLikeToggle(e) {
        const $newCount = document.createElement('div')
        const increment = Number(e.detail.increment)
        const newCount = Number(e.detail.likes_count)

        $newCount.classList.add((increment === 1) ? 'is-up' : 'is-down')
        $newCount.innerText = newCount
        this.$count.innerHTML = ''
        this.$count.appendChild($newCount)

        this.$el.classList.toggle(this.options.classNameIsLiked, (increment === 1));
        this.count = newCount
    }
}
