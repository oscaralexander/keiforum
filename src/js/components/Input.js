export default class Input
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            classNameIsDirty: 'is-dirty',
            classNameIsEmpty: 'is-empty',
            ...options
        }

        this.initListeners()
    }

    initListeners() {
        this.$el.addEventListener('change', this.onInput.bind(this), false)
        this.$el.addEventListener('keydown', this.onInput.bind(this), false)
        window.requestAnimationFrame(this.onInput.bind(this))
    }

    onInput() {
        const isDirty = (this.$el.value !== this.$el.defaultValue)
        const isEmpty = (this.$el.value.length === 0)

        this.$el.classList.toggle(this.options.classNameIsDirty, isDirty)
        this.$el.classList.toggle(this.options.classNameIsEmpty, isEmpty)
    }
}
