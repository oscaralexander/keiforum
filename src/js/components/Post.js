export default class Post
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            selector: '.js-post',
            ...options
        }

        this.init()
        this.initListeners()
    }

    init() {
    }

    initListeners() {
    }
}
