import Upload from './Upload'

export default class AvatarUpload
{
    constructor($el, options = {}) {
        options = {
            allowedTypes: 'image/jpeg,image/png',
            classNameIsBusy: 'is-busy',
            classNameIsUploading: 'is-uploading',
            maxFileSize: 5000000,
            selectorInputFile: 'input[type="file"]',
            selectorInputUrl: 'input[name="upload_url"]',
            selectorUpload: '.js-avatarUploadUpload',
            selectorUploadTemplate: '.js-avatarUploadTemplate',
            ...options
        }

        super($el, options)
    }

    init() {
        super.init()
        this.$inputFile = this.$el.querySelector(this.options.selectorInputFile)
        this.$inputUrl = this.$el.querySelector(this.options.selectorInputUrl)
        this.$upload = this.$el.querySelector(this.options.selectorUpload)
        this.upload = _.template(this.$el.querySelector(this.options.selectorUploadTemplate).innerHTML)
    }

    initListeners() {
        this.$inputFile.addEventListener('change', this.onImageChange.bind(this))
    }

    onImageChange(e) {
        if (e.target.files.length) {
            const file = e.target.files[0]
            this.$inputImage.value = ''

            if (this.validate(file)) {
                const $div = document.createElement('div')
                $div.innerHTML = this.upload({ fileName: file.name })

                const $upload = $div.firstElementChild
                $upload._app ||= {}
                $upload._app.upload = new Upload($upload, file)
                $upload.addEventListener('upload:complete', this.onUploadComplete.bind(this), false)
                $upload.addEventListener('upload:cancel', this.onUploadCancel.bind(this), false)

                this.$el.classList.add(
                    this.options.classNameIsBusy,
                    this.options.classNameIsUploading,
                )
                this.$upload.appendChild($upload)
            }
        }
    }

    onSubmit() {
        this.$el.dispatchEvent(new CustomEvent('avatarupload:submit', {
            detail: {
                url: this.$inputUrl.value.trim()
            }
        }))

        this.hide()
    }

    onUploadCancel(e) {
        console.log('onUploadCancel', e)
    }

    onUploadComplete(e) {
        console.log(e)

        this.$el.dispatchEvent(new CustomEvent('modalimage:submit', {
            detail: {
                url: e.detail.url,
            },
        }))

        this.$el.classList.remove(
            this.options.classNameIsBusy,
            this.options.classNameIsUploading,
        )

        this.$upload.innerHTML = ''
        this.hide()
    }

    validate(file) {
        const allowedTypes = this.options.allowedTypes.split(',')

        if (allowedTypes.indexOf(file.type) === -1) {
            alert('Ongeldig bestandstype.')
            return false
        }

        if (file.size > this.options.maxFileSize) {
            alert('Bestand is te groot.')
            return false
        }

        return true
    }
}