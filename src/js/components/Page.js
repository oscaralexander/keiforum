import { forEach } from "lodash"

export default class Page
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            ...options
        }

        this.init()
        this.initListeners()
    }

    init() {
        this.$$user = this.$el.querySelectorAll('[data-user-id]')

        this.initListeners()
        this.initOnlineStatus()
    }

    initListeners() {
    }

    initOnlineStatus() {
        let userId
        let userIds = [];

        this.$$user.forEach($user => {
            userId = $user.dataset.userId || null

            if (userId !== null && userId !== '') {
                if (!userIds.includes(userId)) {
                    userIds.push(userId)
                }
            }
        })

        const config = {
            params: {
                user_ids: userIds,
            },
        }

        axios.get('/online', config)
            .then(this.onOnlineStatusSuccess.bind(this))
    }

    onOnlineStatusSuccess(response) {
        const onlineUserIds = response.data.online_user_ids

        onlineUserIds.forEach(userId => {
            document.querySelectorAll(`[data-user-id='${userId}']`).forEach($user => {
                $user.classList.add('is-online')
            })
        })
    }
}
