import Modal from './Modal'

export default class ModalLink extends Modal
{
    constructor($el, options = {}) {
        options = {
            selectorInputText: 'input[name="link_text"]',
            selectorInputUrl: 'input[name="link_url"]',
            ...options
        }

        super($el, options)
    }

    init() {
        super.init()
        this.$inputText = this.$el.querySelector(this.options.selectorInputText)
        this.$inputUrl = this.$el.querySelector(this.options.selectorInputUrl)
    }

    initListeners() {
        super.initListeners()
        this.$el.addEventListener('modal:submit', this.onSubmit.bind(this), false)
    }

    onSubmit() {
        this.$el.dispatchEvent(new CustomEvent('modallink:submit', {
            detail: {
                text: this.$inputText.value.trim(),
                url: this.$inputUrl.value.trim(),
            },
        }))

        this.hide()
    }
}
