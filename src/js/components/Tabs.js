export default class Tabs
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            hashPrefix: 'tab-',
            ...options
        }

        this.init()
        this.initListeners()
    }

    init() {
        this.$$tab = this.$el.querySelectorAll(`[role='tab']`)

        const hash = document.location.hash.replace('#', '')
        const regExp = new RegExp(`^tab\-`)

        if (hash.match(regExp)) {
            const tab = hash.replace(regExp, '')
            this.activate(tab)
        }
    }

    initListeners() {
        this.$$tab.forEach($tab => $tab.addEventListener('click', this.onTabClick.bind(this)))
    }

    activate(tab) {
        const $tab = document.getElementById(`tab-${tab}`)
        const $tabPanel = document.getElementById(tab)

        if ($tab && $tabPanel) {
            this.$el.querySelectorAll(`[role='tab']`).forEach($tab => {
                $tab.setAttribute('aria-selected', false)
            })

            this.$el.querySelectorAll(`[role='tabpanel']`).forEach($tabPanel => {
                $tabPanel.setAttribute('hidden', true)
            })

            $tab.setAttribute('aria-selected', true)
            $tabPanel.removeAttribute('hidden')
            document.location.hash = `#tab-${tab}`

            this.$el.dispatchEvent(new CustomEvent('tabs:change', {
                detail: { tab }
            }), { bubbles: true })
        }
    }

    onTabClick(e) {
        e.preventDefault()
        const tab = e.currentTarget.getAttribute('aria-controls')

        if (tab) {
            this.activate(tab)
        }
    }
}
