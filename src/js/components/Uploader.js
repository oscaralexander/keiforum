import Upload from './Upload'

/*
Thumbnail  | Thumbnail            | Thumbnail   | Keeps Image
Suffix     | Name                 | Size        | Proportions
----------------------------------------------------------------
s          | Small Square         | 90x90       | No
b          | Big Square           | 160x160     | No
t          | Small Thumbnail      | 160x160     | Yes
m          | Medium Thumbnail     | 320x320     | Yes
l          | Large Thumbnail      | 640x640     | Yes
h          | Huge Thumbnail       | 1024x1024   | Yes
*/

export default class Uploader
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            allowedTypes: 'image/gif,image/jpeg,image/png',
            classNameIsBusy: 'is-busy',
            classNameIsUploading: 'is-uploading',
            maxUploads: 1,
            selectorInputFile: 'input[type="file"]',
            selectorUploads: '.js-uploaderUploads',
            selectorUploadTemplate: '.js-uploaderUploadTemplate',
            ...options
        }

        if (this.$el.dataset.uploaderAllowedTypes) {
            this.options.allowedTypes = this.$el.dataset.uploaderAllowedTypes
        }

        if (this.$el.dataset.uploaderMaxUploads) {
            this.options.maxUploads = this.$el.dataset.uploaderMaxUploads
        }

        this.init()
        this.initListeners()
    }

    init() {
        this.$inputFile = this.$el.querySelector(this.options.selectorInputFile)
        this.$uploads = this.$el.querySelector(this.options.selectorUploads)
        this.numUploading = 0
        this.numUploads = 0
        this.uploadTemplate = _.template(this.$el.querySelector(this.options.selectorUploadTemplate).innerHTML)
    }

    initListeners() {
        this.$inputFile.addEventListener('change', this.onFileChange.bind(this))
    }

    onFileChange(e) {
        let file

        for (let i = 0; i < e.target.files.length; i++) {
            file = e.target.files[i]

            if (this.numUploads < this.options.maxUploads) {
                this.numUploading += 1
                this.numUploads += 1
                this.upload(file)
            } else {
                break
            }
        }
    }

    onUploadCancel(e) {
        const $upload = e.target

        console.log($upload)

        this.numUploading -= 1
        this.numUploads -= 1

        if (this.numUploading === 0) {
            this.$el.classList.remove(
                this.options.classNameIsBusy,
                this.options.classNameIsUploading,
            )
        }

        this.$uploads.removeChild($upload)
    }

    onUploadComplete(e) {
        this.numUploading -= 1

        if (this.numUploading === 0) {
            this.$el.classList.remove(
                this.options.classNameIsBusy,
                this.options.classNameIsUploading,
            )
        }

        this.$el.dispatchEvent(new CustomEvent('uploader:uploadcomplete', {
            detail: {
                url: e.detail.url,
            },
        }))
    }

    upload(file) {
        if (this.validate(file)) {
            const $ul = document.createElement('ul')
            $ul.innerHTML = this.uploadTemplate({ fileName: file.name })

            const $upload = $ul.firstElementChild
            $upload._app ||= {}
            $upload._app.upload = new Upload($upload, file)
            $upload.addEventListener('upload:cancel', this.onUploadCancel.bind(this), false)
            $upload.addEventListener('upload:complete', this.onUploadComplete.bind(this), false)

            this.$el.classList.add(
                this.options.classNameIsBusy,
                this.options.classNameIsUploading,
            )

            this.$uploads.appendChild($upload)
        }
    }

    validate(file) {
        const allowedTypes = this.options.allowedTypes.split(',')

        if (allowedTypes.indexOf(file.type) === -1) {
            alert('Ongeldig bestandstype.')
            return false
        }

        return true
    }
}