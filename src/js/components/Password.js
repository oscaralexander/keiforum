export default class Password
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            classNameIsVisible: 'is-visible',
            selector: '.js-password',
            selectorToggle: '.js-passwordToggle',
            ...options
        }

        this.init()
        this.initListeners()
    }

    init() {
        this.$input = this.$el.querySelector('input[type="password"]')
        this.$toggle = this.$el.querySelector(this.options.selectorToggle)
        this.isVisible = false
    }

    initListeners() {
        this.$toggle.addEventListener('click', this.onToggle.bind(this), false)
    }

    onToggle() {
        this.isVisible = !this.isVisible
        this.$input.setAttribute('type', this.isVisible ? 'text' : 'password')
        this.$el.classList.toggle(this.options.classNameIsVisible, this.isVisible)
        this.$input.focus()
    }
}
