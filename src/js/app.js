import './bootstrap'

import Editor from './components/Editor'
import Input from './components/Input'
import InputSelect from './components/InputSelect'
import Likes from './components/Likes'
import Modal from './components/Modal'
import ModalImage from './components/ModalImage'
import ModalLink from './components/ModalLink'
import Page from './components/Page'
import Pagination from './components/Pagination'
import Password from './components/Password'
import PasswordRequirements from './components/PasswordRequirements'
import Post from './components/Post'
import Tabs from './components/Tabs'
import Textarea from './components/Textarea'
import Uploader from './components/Uploader'
import XhrForm from './components/XhrForm'

window.addEventListener('DOMContentLoaded', e => {
    window.app.init([
        { 'name': 'editor', 'obj': Editor },
        { 'name': 'input', 'obj': Input },
        { 'name': 'inputSelect', 'obj': InputSelect },
        { 'name': 'likes', 'obj': Likes },
        { 'name': 'modal', 'obj': Modal },
        { 'name': 'modalImage', 'obj': ModalImage },
        { 'name': 'modalLink', 'obj': ModalLink },
        { 'name': 'page', 'obj': Page },
        { 'name': 'pagination', 'obj': Pagination },
        { 'name': 'password', 'obj': Password },
        { 'name': 'passwordRequirements', 'obj': PasswordRequirements },
        { 'name': 'post', 'obj': Post },
        { 'name': 'tabs', 'obj': Tabs },
        { 'name': 'textarea', 'obj': Textarea },
        { 'name': 'uploader', 'obj': Uploader },
        { 'name': 'xhrForm', 'obj': XhrForm },
    ])
})