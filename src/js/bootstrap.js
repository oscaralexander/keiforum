window.APP_SYMBOL = Symbol.for('app')

window.app = {
    bindings: [],

    bind($root, name, obj, options) {
        $root.querySelectorAll(`.js-${name}`).forEach($el => {
            $el._app ||= {}

            if ($el._app[name] === undefined) {
                $el._app[name] = new obj($el, options)
            }
        })
    },

    bindAll($root) {
        this.bindings.forEach(binding => {
            this.bind($root, binding.name, binding.obj, binding.options || {})
        })
    },

    getCsrfToken() {
        const $token = document.head.querySelector('meta[name=csrf_token]')
        const token = $token ? $token.content : null

        console.info('Get CSRF token: ' + token)

        return token
    },

    init(bindings) {
        this.bindings = bindings

        const mutationObserver = new MutationObserver((mutations) => {
            let targets = []

            mutations.forEach(mutationRecord => {
                if (!targets.includes(mutationRecord.target)) {
                    this.bindAll(mutationRecord.target)
                    targets.push(mutationRecord.target)
                }
            })

            document.body.dispatchEvent(new CustomEvent('domchanged', {
                detail: {
                    targets
                }
            }))
        })

        mutationObserver.observe(document.documentElement, {
            childList: true,
            subtree: true,
        })

        this.bindAll(document)
    },

    setCsrfToken(token) {
        const $$input = document.querySelectorAll('input[name=csrf_token]')
        const $meta = document.head.querySelector('meta[name=csrf_token]')

        console.info('Set CSRF token: ' + token)

        $$input.forEach($input => {
            $input.value = token
        })
        
        if ($meta) {
            $meta.content = token
        }
    },
}

if (window.axios) {
    axios.interceptors.request.use(config => {
        config.headers['X-CSRF-Token'] = window.app.getCsrfToken()
        return config
    })

    axios.interceptors.response.use(response => {
        window.app.setCsrfToken(response.headers['x-csrf-token'] || null)
        return response
    }, error => {
        window.app.setCsrfToken(error.response.headers['x-csrf-token'] || null)
        return Promise.reject(error)
    })
}
